﻿
using Shared.Enumetarions.Definitions;
using System.Collections.Generic;

namespace Shared.Caches
{
    public class CacheKey
    {

        public string Name { get; set; }

        public int OperationTypeId { get; set; }

        public OperationTypeEnum OperationType { get; set; }

        public List<string> ParentPermissionName { get; set; }

    }
}
