﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Services
{
    public interface ISharedIdentityService
    {
        public string GetUserId { get; }

        public string GetOrganizationId { get; }

        public List<string> GetRoleIds { get; }
    }
}
