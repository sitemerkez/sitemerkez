﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Services
{
    /// <summary>
    /// Encryption service
    /// </summary>
    public interface IEncryptionService
    {
        string CreateSaltKey(int size);

        string CreatePasswordHash(string password, string saltkey, string passwordFormat);

        string CreateHash(byte[] data, string hashAlgorithm);

        string EncryptText(string plainText, string encryptionPrivateKey);

        string DecryptText(string cipherText, string encryptionPrivateKey);
    }
}
