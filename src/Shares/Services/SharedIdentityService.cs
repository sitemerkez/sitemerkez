﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Services
{
    public class SharedIdentityService : ISharedIdentityService
    {
        private IHttpContextAccessor _httpContextAccessor;

        public SharedIdentityService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserId => _httpContextAccessor.HttpContext.User.FindFirst("userId").Value;

        public string GetOrganizationId => _httpContextAccessor.HttpContext.User.FindFirst("organizationId").Value;

        public List<string> GetRoleIds => _httpContextAccessor.HttpContext.User.FindAll("roleIds").Select(p=> p.ToString()).ToList();
    }
}
