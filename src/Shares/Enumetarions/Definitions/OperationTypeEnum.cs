﻿using Shared.Enumetarions.Commons;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enumetarions.Definitions
{
    public enum OperationTypeEnum
    {
        Insert = 1,
        Select = 2,
        Update = 3,
        Delete = 4,
        Approve = 5,
        Reject = 6,
        Active = 7,
        Deactivate = 8,
        Manage = 9,

    }
}