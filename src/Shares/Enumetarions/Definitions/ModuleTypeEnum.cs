﻿using Shared.Enumetarions.Commons;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enumetarions.Definitions
{
    public enum ModuleTypeEnum  
    {
         User = 1,
         HR = 2,
         CRM = 3,
         Accounting  = 4,
         EShopping = 5

    }
}
