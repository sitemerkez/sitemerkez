﻿using Shared.Enumetarions.Commons;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enumetarions.Definitions
{
    public enum SystemTypeEnum
    {
        User = 1,
        Schedule = 2
    }
}
