﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enumetarions.Definitions
{
    public enum DataTypeEnum
    {
        [Display(Name = "Text")]
        Text = 1,

        [Display(Name = "TextLimited")]
        TextLimited = 2,

        [Display(Name = "Int")]
        Int = 3,

        [Display(Name = "Decimal")]
        Decimal = 4,

        [Display(Name = "Datetime")]
        Datetime = 5,

        [Display(Name = "Bit")]
        Bit = 6,
    }
}
