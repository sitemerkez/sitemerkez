﻿using Shared.Enumetarions.Commons;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enumetarions.Definitions
{
    public enum RoleTypeEnum 
    {
        User = 1,
        SuperAdmin = 2, 
        Admin = 3,
        Customer = 4,
        Chief = 5,
        TimeKeeper = 6,
    }
}
