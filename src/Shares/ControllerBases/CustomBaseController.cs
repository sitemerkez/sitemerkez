﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.ControllerBases
{
    public class CustomBaseController : ControllerBase
    {

        protected readonly IMediator _mediator;
        protected IConfiguration _configuration;
        protected readonly ISharedIdentityService _sharedIdentityService;
        protected readonly IMemoryCache _memoryCache;

        public CustomBaseController(IMediator mediator, 
            IConfiguration configuration, 
            ISharedIdentityService sharedIdentityService,
            IMemoryCache memoryCache)
        {
            _mediator = mediator;
            _configuration = configuration;
            _sharedIdentityService = sharedIdentityService;
            _memoryCache = memoryCache;
        }
    }
}
