﻿using IdentityService.Application.Models.Authetications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityService.Application.Features.Authentications.Roles.Queries.GetRolesList
{
    public class GetRolesListByUserIdQuery : IRequest<IEnumerable<RoleModel>>
    {
        public Guid UserId { get; set; }

        public GetRolesListByUserIdQuery(Guid userId)
        {
            UserId = userId;

        }
    }
}
