﻿using AutoMapper;
using IdentityService.Application.Models.Authetications;
using IdentityService.Domain.Entities.Authentications;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IdentityService.Application.Features.Authentications.Roles.Queries.GetRolesList
{


    internal class GetRolesListByUserIdQueryHandler : IRequestHandler<GetRolesListByUserIdQuery, IEnumerable<RoleModel>>
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IMapper _mapper;

        public GetRolesListByUserIdQueryHandler(
                 UserManager<User> userManager,
                 RoleManager<Role> roleManager,
                 IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;

        }
        public async Task<IEnumerable<RoleModel>> Handle(GetRolesListByUserIdQuery request, CancellationToken cancellationToken)
        {
            var user = _userManager.Users.Where(p => p.Id == request.UserId).FirstOrDefault();

            var roleList = new List<Role>();
            var roles = _roleManager.Roles.ToList();

            var userRoles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                if (userRoles.Any(p => p == role.Name))
                {
                    roleList.Add(role);
                }
            }

            var roleModel = _mapper.Map<IEnumerable<RoleModel>>(roleList);

            return roleModel;
        }
    }
}
