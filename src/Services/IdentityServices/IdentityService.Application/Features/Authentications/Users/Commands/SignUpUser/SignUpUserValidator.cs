﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityService.Application.Features.Authentications.Users.Commands.SignUpUser
{

    public class SignUpUserValidator : AbstractValidator<SignUpUserCommand>
    {
        public SignUpUserValidator()
        {

            RuleFor(p => p.Email).NotEmpty().WithMessage("{EmailAddress} is required.");

            RuleFor(p => p.Password).NotEmpty().WithMessage("{EmailAddress} is required.");

        }
    }
}
