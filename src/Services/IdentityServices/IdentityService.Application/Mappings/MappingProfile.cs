﻿using AutoMapper;
using IdentityService.Application.Features.Authentications.Users.Commands.SignUpUser;
using IdentityService.Application.Models.Authetications;
using IdentityService.Domain.Entities.Authentications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityService.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, SignUpUserCommand>().ReverseMap();
            CreateMap<User, UserModel>().ReverseMap();

            CreateMap<Role, RoleModel>().ReverseMap();
            CreateMap<RoleModel, Role>().ReverseMap();
        }
    }
}
