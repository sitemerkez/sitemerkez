﻿using AutoMapper;
using IdentityService.Application.Features.Authentications.Users.Commands.SignUpUser;
using IdentityService.Application.Features.Authentications.Users.Queries.GetUser;
using IdentityService.Application.Models.Authetications;
using IdentityService.Domain.Entities.Authentications;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Settings.Authentications;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IdentityService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly JwtSettings _jwtSettings;
        private readonly SignInManager<User> _signInManager;


        public AuthController(IMediator mediator,
            IOptionsSnapshot<JwtSettings> jwtSettings,
            SignInManager<User> signInManager)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _jwtSettings = jwtSettings.Value;
            _signInManager = signInManager;
        }

        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(SignUpUserCommand signUpUserCommand)
        {
            var result = await _mediator.Send(signUpUserCommand);

            if (result != "")
            {
                return Ok();
            }

            return BadRequest("User not created");
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(string email, string password)
        {
            var query = new GetUserByEmailAndPasswordQuery(email, password);
            var userModel = await _mediator.Send(query);

            if (userModel != null)
            {
                return Ok(GenerateJwt(userModel));
            }

            return BadRequest("Email or password incorrect.");
        }

        //[HttpPost("GoogleLogin")]
        //public IActionResult GoogleLogin(string ReturnUrl)
        //{
        //    string redirectUrl = Url.Action("ExternalResponse", "User", new { ReturnUrl = ReturnUrl });
        //    //Google'a yapılan Login talebi neticesinde kullanıcıyı yönlendirmesini istediğimiz url'i oluşturuyoruz.
        //    AuthenticationProperties properties = _signInManager.ConfigureExternalAuthenticationProperties("Google", redirectUrl);
        //    //Bağlantı kurulacak harici platformun hangisi olduğunu belirtiyor ve bağlantı özelliklerini elde ediyoruz.
        //    return new ChallengeResult("Google", properties);
        //    //ChallengeResult; kimlik doğrulamak için gerekli olan tüm özellikleri kapsayan AuthenticationProperties nesnesini alır ve ayarlar.
        //}

        //    [HttpPost("Roles")]
        //    public async Task<IActionResult> CreateRole(string roleName)
        //    {
        //        if (string.IsNullOrWhiteSpace(roleName))
        //        {
        //            return BadRequest("Role name should be provided.");
        //        }

        //        var newRole = new Role
        //        {
        //            Name = roleName
        //        };

        //        var roleResult = await _roleManager.CreateAsync(newRole);

        //        if (roleResult.Succeeded)
        //        {
        //            return Ok();
        //        }

        //        return Problem(roleResult.Errors.First().Description, null, 500);
        //    }

        //    [HttpPost("User/{userEmail}/Role")]
        //    public async Task<IActionResult> AddUserToRole(string userEmail, [FromBody] string roleName)
        //    {
        //        var user = _userManager.Users.SingleOrDefault(u => u.UserName == userEmail);

        //        var result = await _userManager.AddToRoleAsync(user, roleName);

        //        if (result.Succeeded)
        //        {
        //            return Ok();
        //        }

        //        return Problem(result.Errors.First().Description, null, 500);
        //    }

        private string GenerateJwt(UserModel userModel)
        {
            var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, userModel.Id.ToString()),
                    new Claim(ClaimTypes.Name, userModel.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, userModel.Id.ToString())
                };

            var roleClaims = userModel.Roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
