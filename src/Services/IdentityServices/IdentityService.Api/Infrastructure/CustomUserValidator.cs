﻿using IdentityService.Domain.Entities.Authentications;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace IdentityService.Api.Infrastructure
{
    public class CustomUserValidator : IUserValidator<User>
    {
        public Task<IdentityResult> ValidateAsync(UserManager<User> manager, User user)
        {
            return Task.FromResult(IdentityResult.Success);

            //if (user.Email.ToLower().EndsWith("@gmail.com") || user.Email.ToLower().EndsWith("@hotmail.com"))
            //{
            //    return Task.FromResult(IdentityResult.Success);
            //}
            //else
            //{
            //    return Task.FromResult(IdentityResult.Failed(new IdentityError()
            //    {
            //        Code = "EmailDomainError",
            //        Description = "Sadece @gmail.com ve @hotmail.com a izin veriliyor."
            //    }));
            //}
        }
    }
}
