﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Domain.Entities.Concrete.Abstarct.Commons
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }

    public interface IEntity
    {
    }
}
