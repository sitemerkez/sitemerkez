﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Domain.Entities.Concrete.Abstarct.Commons
{
    public interface ISeoEntity
    {
        string Title { get; set; }
        string MetaDescription { get; set; }
        string MetaKeywords { get; set; }
    }
}
