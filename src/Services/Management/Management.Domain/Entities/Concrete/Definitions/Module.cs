﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Organizations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class Module : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity, IDisplayEntity
    {
        public Module()
        {
            OrganizationModules = new HashSet<OrganizationModule>();
        }

        [ForeignKey("FK_ModuleType")]
        public int ModuleTypeId { get; set; }

        public string Description { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public int DisplayOrder { get; set; }
        public bool IsDisplay { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }
        public virtual ModuleType ModuleType { get; set; }

        public virtual ICollection<OrganizationModule> OrganizationModules { get; set; }
       
    }
}
