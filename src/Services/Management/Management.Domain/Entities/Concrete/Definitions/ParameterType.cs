﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Parameters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class ParameterType : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity
    {
        public ParameterType()
        {
            Parameters = new HashSet<Parameter>();
        }

        [Required]
        public int ParameterTypeEnumId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<Parameter> Parameters { get; set; }
    }
}

