﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class Language : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity, IDisplayEntity
    {
        public Language()
        {
            Translates = new HashSet<Translate>();
        }

        [Required]
        public string Code { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsDisplay { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<Translate> Translates { get; set; }
    }
}
