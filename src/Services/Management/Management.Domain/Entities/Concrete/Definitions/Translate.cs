﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class Translate : BaseEntity, ICreatedEntity, ILastModifiableEntity, IDisplayEntity
    {
        [Required]
        [ForeignKey("Language")]
        public int LanguageId { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Value { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsDisplay { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual Language Language { get; set; }
        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }
    }
}
