﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class ModuleType : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity, IDisplayEntity
    {
        public ModuleType()
        {
            Modules = new HashSet<Module>();
        }
        [Required]
        public int ModuleTypeEnumId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<Module> Modules { get; set; }
       
        public int DisplayOrder { get; set; }
        public bool IsDisplay { get; set; }
    }
}

