﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Parameters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Definitions
{
    public class SystemType : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity
    {
        public SystemType()
        {
            ParameterSystemTypes = new HashSet<ParameterSystemType>();
        }
        [Required]
        public int SystemTypeEnumId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<ParameterSystemType> ParameterSystemTypes { get; set; }
    }
}

