﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataType = Management.Domain.Entities.Concrete.Definitions.DataType;

namespace Management.Domain.Entities.Concrete.Parameters
{
    public class Parameter : BaseEntityWithName, ICreatedEntity, ILastModifiableEntity
    {
        [ForeignKey("FK_Organization")]
        public int? OrganizationId { get; set; }

        [Required]
        [ForeignKey("FK_DataType")]
        public int DataTypeId { get; set; }

        [Required]
        [ForeignKey("FK_ParameterType")]
        public int ParameterTypeId { get; set; }

        public string Description { get; set; }

        [Required]
        public int Maximum { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }
        public virtual DataType DataType { get; set; }
        public virtual ParameterType ParameterType { get; set; }
      
       
    }
}
