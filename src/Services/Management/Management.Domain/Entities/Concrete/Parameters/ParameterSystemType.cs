﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using Shared.Enumetarions.Definitions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Parameters
{
    public class ParameterSystemType : BaseEntity, ICreatedEntity, ILastModifiableEntity
    {
        public ParameterSystemType()
        {
            Parameters = new HashSet<Parameter>();
        }
        [Required]
        [ForeignKey(nameof(User))]
        public int ParameterId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int SystemTypeId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual Parameter Parameter { get; set; }
        public virtual SystemType SystemType { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<Parameter> Parameters { get; set; }
    }
}
