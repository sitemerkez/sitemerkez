﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Commons;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Authentications
{
    public class RolePermission : BaseEntity, ICreatedEntity, ILastModifiableEntity
    {
        [ForeignKey("FK_Organization")]
        public int? OrganizationId { get; set; }
       
        public string RoleId { get; set; }

        [Required]
        [ForeignKey("FK_Permission")]
        public int PermissionId { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }
        public virtual Permission Permission { get; set; }
    }
}
