﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Authentications
{
    public class Permission : BaseEntityWithName, IDisplayEntity, ICreatedEntity, ILastModifiableEntity
    {
        public Permission()
        {
            Children = new HashSet<Permission>();
            RolePermissions = new HashSet<RolePermission>();
        }
        [Required]
        [ForeignKey("FK_OperationType")]
        public int OperationTypeId { get; set; }

        public int? ParentId { get; set; }

        public virtual OperationType OperationType { get; set; }
        public virtual Permission Parent { get; set; }
        
        public virtual ICollection<Permission> Children { get; set; }
        public virtual ICollection<RolePermission> RolePermissions { get; set; }

        public int DisplayOrder { get; set; }
        public bool IsDisplay { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }
    }
}
