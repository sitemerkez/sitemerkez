﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using Management.Domain.Entities.Concrete.Organizations;
using Management.Domain.Entities.Concrete.Parameters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataType = Management.Domain.Entities.Concrete.Definitions.DataType;

namespace Management.Domain.Entities.Concrete.Authentications
{
    public class User : BaseEntity, ICreatedEntity, ILastModifiableEntity, IDisplayEntity
    {
        public User()
        {
            CreatedUsers = new HashSet<User>();
            LastModifiedUsers = new HashSet<User>();

            #region Authentications

            CreatedPermissions = new HashSet<Permission>();
            LastModifiedPermissions = new HashSet<Permission>();

            CreatedRolePermissions = new HashSet<RolePermission>();
            LastModifiedRolePermissions = new HashSet<RolePermission>();

            #endregion

            #region Definitions

            CreatedLanguages = new HashSet<Language>();
            LastModifiedLanguages = new HashSet<Language>();

            CreatedTranslates = new HashSet<Translate>();
            LastModifiedTranslates = new HashSet<Translate>();

            CreatedModuleTypes = new HashSet<ModuleType>();
            LastModifiedModuleTypes = new HashSet<ModuleType>();

            CreatedUsers = new HashSet<User>();
            LastModifiedUsers = new HashSet<User>();

            CreatedModules = new HashSet<Module>();
            LastModifiedModules = new HashSet<Module>();

            CreatedDataTypes = new HashSet<DataType>();
            LastModifiedDataTypes = new HashSet<DataType>();

            CreatedOperationTypes = new HashSet<OperationType>();
            LastModifiedOperationTypes = new HashSet<OperationType>();

            CreatedParameterTypes = new HashSet<ParameterType>();
            LastModifiedParameterTypes = new HashSet<ParameterType>();

            CreatedSystemTypes = new HashSet<SystemType>();
            LastModifiedSystemTypes = new HashSet<SystemType>();

            #endregion

            #region Organizations

            CreatedOrganizations = new HashSet<Organization>();
            LastModifiedOrganizations = new HashSet<Organization>();

            //CreatedOrganizationModules = new HashSet<OrganizationModule>();
            //LastModifiedOrganizationModules = new HashSet<OrganizationModule>();

            #endregion

            #region Parameters

            CreatedParameters = new HashSet<Parameter>();
            LastModifiedParameters = new HashSet<Parameter>();

            CreatedParameterSystemTypes = new HashSet<ParameterSystemType>();
            LastModifiedParameterSystemTypes = new HashSet<ParameterSystemType>();


            #endregion
        }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Surname { get; set; }

        [Required]
        [StringLength(100)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string MobilePhone { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string PasswordSalt { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsDisplay { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public int CreatedUserId { get; set; }

        [ForeignKey(nameof(User))]
        public int? LastModifiedUserId { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User LastModifiedUser { get; set; }

        public virtual ICollection<User> CreatedUsers { get; set; }
        public virtual ICollection<User> LastModifiedUsers { get; set; }

        #region Authentications

        public virtual ICollection<Permission> CreatedPermissions { get; set; }
        public virtual ICollection<Permission> LastModifiedPermissions { get; set; }

        public virtual ICollection<RolePermission> CreatedRolePermissions { get; set; }
        public virtual ICollection<RolePermission> LastModifiedRolePermissions { get; set; }

        #endregion

        #region Definitions

        public virtual ICollection<Language> CreatedLanguages { get; set; }
        public virtual ICollection<Language> LastModifiedLanguages { get; set; }

        public virtual ICollection<Translate> CreatedTranslates { get; set; }
        public virtual ICollection<Translate> LastModifiedTranslates { get; set; }

        public virtual ICollection<ModuleType> CreatedModuleTypes { get; set; }
        public virtual ICollection<ModuleType> LastModifiedModuleTypes { get; set; }

        public virtual ICollection<Module> CreatedModules { get; set; }
        public virtual ICollection<Module> LastModifiedModules { get; set; }

        public virtual ICollection<DataType> CreatedDataTypes { get; set; }
        public virtual ICollection<DataType> LastModifiedDataTypes { get; set; }

        public virtual ICollection<OperationType> CreatedOperationTypes { get; set; }
        public virtual ICollection<OperationType> LastModifiedOperationTypes { get; set; }

        public virtual ICollection<ParameterType> CreatedParameterTypes { get; set; }
        public virtual ICollection<ParameterType> LastModifiedParameterTypes { get; set; }

        public virtual ICollection<SystemType> CreatedSystemTypes { get; set; }
        public virtual ICollection<SystemType> LastModifiedSystemTypes { get; set; }

        #endregion

        #region Organizations

        public virtual ICollection<Organization> CreatedOrganizations { get; set; }
        public virtual ICollection<Organization> LastModifiedOrganizations { get; set; }

        //public virtual ICollection<OrganizationModule> CreatedOrganizationModules { get; set; }
        //public virtual ICollection<OrganizationModule> LastModifiedOrganizationModules { get; set; }



        #endregion

        #region Parameters

        public virtual ICollection<Parameter> CreatedParameters { get; set; }
        public virtual ICollection<Parameter> LastModifiedParameters { get; set; }

        public virtual ICollection<ParameterSystemType> CreatedParameterSystemTypes { get; set; }
        public virtual ICollection<ParameterSystemType> LastModifiedParameterSystemTypes { get; set; }

        #endregion


    }
}
