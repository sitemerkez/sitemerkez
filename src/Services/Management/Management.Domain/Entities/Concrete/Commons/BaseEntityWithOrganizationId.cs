﻿using Management.Domain.Entities.Concrete.Organizations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Commons
{
    public abstract class BaseEntityWithOrganizationId : BaseEntity
    {
        [ForeignKey("FK_Organization")]
        public int OrganizationId { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
