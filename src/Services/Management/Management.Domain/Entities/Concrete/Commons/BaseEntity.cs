﻿using Management.Domain.Entities.Concrete.Abstarct.Commons;
using System;
using System.ComponentModel.DataAnnotations;

namespace Management.Domain.Entities.Concrete.Commons
{
    public abstract class BaseEntity : IAggregateRoot
    {
        [Required]
        [Key]
        public int Id { get; protected set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }
}
