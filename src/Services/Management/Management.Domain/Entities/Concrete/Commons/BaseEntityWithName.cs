﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Domain.Entities.Concrete.Commons
{
    public abstract class BaseEntityWithName : BaseEntity
    {
        [Required]
        public string Name { get; set; }
    }
}
