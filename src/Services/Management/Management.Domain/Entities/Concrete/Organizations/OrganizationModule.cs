﻿
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Domain.Entities.Concrete.Organizations
{
    public class OrganizationModule : BaseEntity
    {
        [Required]
        [ForeignKey("FK_Module")]
        public int ModuleId { get; set; }

        [ForeignKey("FK_Organization")]
        public int OrganizationId { get; set; }

        [Required]
        public DateTime BaslangicTarih { get; set; }

        public DateTime? BitisTarih { get; set; }

        //[Required]
        //[ForeignKey(nameof(User))]
        //public int CreatedUserId { get; set; }

        //[ForeignKey(nameof(User))]
        //public int? LastModifiedUserId { get; set; }

        //public virtual User CreatedUser { get; set; }
        //public virtual User LastModifiedUser { get; set; }

        public virtual Module Module { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
