﻿using Management.Domain.Entities.Concrete.Organizations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Domain.Configurations.Organizations
{
    public class OrganizationModuleConfiguration : IEntityTypeConfiguration<OrganizationModule>
    {
        public void Configure(EntityTypeBuilder<OrganizationModule> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.Organization)
                  .WithMany(p => p.OrganizationModules)
                  .HasForeignKey(d => d.OrganizationId)
                  .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Module)
                   .WithMany(p => p.OrganizationModules)
                   .HasForeignKey(d => d.ModuleId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            //entity.HasOne(d => d.CreatedUser)
            //       .WithMany(p => p.CreatedOrganizationModules)
            //       .HasForeignKey(d => d.CreatedUserId)
            //       .OnDelete(DeleteBehavior.ClientSetNull);

            //entity.HasOne(d => d.LastModifiedUser)
            //       .WithMany(p => p.LastModifiedOrganizationModules)
            //       .HasForeignKey(d => d.LastModifiedUserId)
            //       .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => new { e.OrganizationId, e.Module, e.BaslangicTarih, e.BitisTarih }, "IX_All_Columns");

            #endregion


        }
    }
}
