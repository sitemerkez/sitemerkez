﻿
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.Domain.Configurations.Commons
{
    //
    // Summary:
    //     Allows configuration for an entity type to be factored into a separate class,
    //     rather than in-line in Microsoft.EntityFrameworkCore.DbContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder).
    //     Implement this interface, applying configuration for the entity in the Microsoft.EntityFrameworkCore.IEntityTypeConfiguration`1.Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder{`0})
    //     method, and then apply the configuration to the model using Microsoft.EntityFrameworkCore.ModelBuilder.ApplyConfiguration``1(Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{``0})
    //     in Microsoft.EntityFrameworkCore.DbContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder).
    //
    // Type parameters:
    //   TEntity:
    //     The entity type to be configured.
    public interface IEntityTypeConfiguration<TEntity> where TEntity : class
    {
        //
        // Summary:
        //     Configures the entity of type TEntity.
        //
        // Parameters:
        //   builder:
        //     The builder to be used to configure the entity type.
        void Configure(EntityTypeBuilder<TEntity> builder);
    }
}
