﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Management.Domain.Entities.Concrete.Definitions;
namespace Management.Domain.Configurations.Definitions
{
    public class DataTypeConfiguration : IEntityTypeConfiguration<DataType>
    {
        public void Configure(EntityTypeBuilder<DataType> entity)
        {
            entity.Property(x => x.Id).IsRequired();
            entity.Property(x => x.CreatedDate).IsRequired();
            entity.Property(x => x.LastModifiedDate).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedDataTypes)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                   .WithMany(p => p.LastModifiedDataTypes)
                   .HasForeignKey(d => d.LastModifiedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => e.Name, "UIX_Name").IsUnique();
            entity.HasIndex(e => e.DataTypeEnumId, "UIX_DataTypeEnumId").IsUnique();

            #endregion


        }
    }
}
