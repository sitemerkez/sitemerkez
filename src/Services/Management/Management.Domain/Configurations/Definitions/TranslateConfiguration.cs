﻿using Management.Domain.Entities.Concrete.Definitions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.Domain.Configurations.Definitions
{
    public class TranslateConfiguration : IEntityTypeConfiguration<Translate>
    {
        public void Configure(EntityTypeBuilder<Translate> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.Language)
                   .WithMany(p => p.Translates)
                   .HasForeignKey(d => d.LanguageId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedTranslates)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                  .WithMany(p => p.LastModifiedTranslates)
                  .HasForeignKey(d => d.LastModifiedUserId)
                  .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => e.Code);
            entity.HasIndex(e => new { e.LanguageId, e.Code }, "UIX_LanguageId_Code").IsUnique();

            #endregion


        }
    }
}
