﻿using Management.Domain.Entities.Concrete.Definitions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.Domain.Configurations.Definitions
{
    public class SystemTypeConfiguration : IEntityTypeConfiguration<SystemType>
    {
        public void Configure(EntityTypeBuilder<SystemType> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedSystemTypes)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                   .WithMany(p => p.LastModifiedSystemTypes)
                   .HasForeignKey(d => d.LastModifiedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => e.Name, "UIX_Name").IsUnique();
            entity.HasIndex(e => e.SystemTypeEnumId, "UIX_SystemTypeEnumId").IsUnique();

            #endregion


        }
    }
}
