﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Management.Domain.Entities.Concrete.Authentications;

namespace Management.Domain.Configurations.Authentications
{
    public class RolePermissionConfiguration : IEntityTypeConfiguration<RolePermission>
    {
        public void Configure(EntityTypeBuilder<RolePermission> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.Permission)
                   .WithMany(p => p.RolePermissions)
                   .HasForeignKey(d => d.PermissionId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedRolePermissions)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                   .WithMany(p => p.LastModifiedRolePermissions)
                   .HasForeignKey(d => d.LastModifiedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => new { e.RoleId, e.PermissionId }, "UIX_RoleId_PermissionId").IsUnique();

            #endregion


        }
    }
}
