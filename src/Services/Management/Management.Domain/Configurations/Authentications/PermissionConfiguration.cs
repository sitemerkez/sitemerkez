﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Management.Domain.Entities.Concrete.Authentications;

namespace Management.Domain.Configurations.Authentications
{
    public class PermissionConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.OperationType)
                   .WithMany(p => p.Permissions)
                   .HasForeignKey(d => d.OperationTypeId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasMany(p => p.Children)
            .WithOne(p => p.Parent)
            .HasForeignKey(p => p.ParentId);

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedPermissions)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                   .WithMany(p => p.LastModifiedPermissions)
                   .HasForeignKey(d => d.LastModifiedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => e.Name, "UIX_Name").IsUnique();
            entity.HasIndex(e => e.ParentId);



            #endregion


        }
    }
}
