﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Management.Domain.Entities.Concrete.Authentications;

namespace Management.Domain.Configurations.Authentications
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.CreatedUser)
              .WithMany(p => p.CreatedUsers)
              .HasForeignKey(d => d.CreatedUserId)
              .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                   .WithMany(p => p.LastModifiedUsers)
                   .HasForeignKey(d => d.LastModifiedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => e.Email, "UIX_Email").IsUnique();
            entity.HasIndex(e => e.UserName, "UIX_UserName").IsUnique();
            entity.HasIndex(e => new { e.Email, e.UserName });

            #endregion


        }
    }


}
