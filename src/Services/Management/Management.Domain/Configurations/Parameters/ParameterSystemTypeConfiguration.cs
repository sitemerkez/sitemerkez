﻿using Management.Domain.Entities.Concrete.Parameters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.Domain.Configurations.Parameters
{
    public class ParameterSystemTypeConfiguration : IEntityTypeConfiguration<ParameterSystemType>
    {
        public void Configure(EntityTypeBuilder<ParameterSystemType> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedParameterSystemTypes)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                  .WithMany(p => p.LastModifiedParameterSystemTypes)
                  .HasForeignKey(d => d.LastModifiedUserId)
                  .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => new { e.ParameterId, e.SystemTypeId }, "UIX_ParameterId_SystemTypeId").IsUnique();

            #endregion


        }
    }
}
