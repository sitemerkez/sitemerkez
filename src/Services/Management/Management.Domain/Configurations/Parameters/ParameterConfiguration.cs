﻿using Management.Domain.Entities.Concrete.Parameters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.Domain.Configurations.Parameters
{
    public class ParameterConfiguration : IEntityTypeConfiguration<Parameter>
    {
        public void Configure(EntityTypeBuilder<Parameter> entity)
        {
            entity.Property(x => x.Id).IsRequired();

            #region ForeingKey

            entity.HasOne(d => d.DataType)
                   .WithMany(p => p.Parameters)
                   .HasForeignKey(d => d.DataTypeId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.ParameterType)
                   .WithMany(p => p.Parameters)
                   .HasForeignKey(d => d.ParameterTypeId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.CreatedUser)
                   .WithMany(p => p.CreatedParameters)
                   .HasForeignKey(d => d.CreatedUserId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.LastModifiedUser)
                  .WithMany(p => p.LastModifiedParameters)
                  .HasForeignKey(d => d.LastModifiedUserId)
                  .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion

            #region Index

            entity.HasIndex(e => new { e.OrganizationId, e.Name }, "UIX_OrganizationId_Name").IsUnique();

            #endregion


        }
    }
}
