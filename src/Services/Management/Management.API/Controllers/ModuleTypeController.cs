﻿using Management.Application.Features.Definitions.Commands.Modules.AddModule;
using Management.Application.Features.Definitions.Commands.Modules.DeleteModule;
using Management.Application.Features.Definitions.Commands.Modules.UpdateModule;
using Management.Application.Features.Definitions.Queries.ModuleTypes.Get.GetModuleTypeById;
using Management.Application.Features.Definitions.Queries.ModuleTypes.GetList;
using Management.Application.Models.Definitions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ModuleTypeController : CustomBaseController
    {
        private readonly ILogger<ModuleTypeController> _logger;

        public ModuleTypeController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<ModuleTypeController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }

        [HttpGet]
        //[ProducesResponseType(typeof(IEnumerable<ModuleTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleTypeViewModel>>> GetAllModuleTypesList()
        {
            var query = new GetAllModuleTypesListQuery();
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{filterText}")]
        [ProducesResponseType(typeof(IEnumerable<ModuleTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleTypeViewModel>>> GetModuleTypesListByFilterText(string filterText)
        {
            var query = new GetModuleTypesListByFilterTextQuery(filterText);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{isDisplay}")]
        [ProducesResponseType(typeof(IEnumerable<ModuleTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleTypeViewModel>>> GetModuleTypesListByIsDisplay(bool IsDisplay)
        {
            var query = new GetModuleTypesListByIsDisplayQuery(IsDisplay);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ModuleTypeViewModel), (int)HttpStatusCode.OK)]
        //[PermissionAuthorizeFilter(permissionName: PermissionProvider.ModuleTypeGetName)]
        public async Task<ActionResult<ModuleTypeViewModel>> GetModuleTypeById(int id)
        {

            var query = new GetModuleTypeByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"ModuleType with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<ModuleTypeViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<ModuleTypeViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<ModuleTypeViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<ModuleTypeViewModel>().Count();

        //    var itemsOnPage = await (new List<ModuleTypeViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<ModuleTypeViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddModuleType(AddModuleTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateModuleType(UpdateModuleTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteModuleType(DeleteModuleTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
