﻿using Management.Application.Features.Organizations.Commands.Organizations.AddOrganization;
using Management.Application.Features.Organizations.Commands.Organizations.DeleteOrganization;
using Management.Application.Features.Organizations.Commands.Organizations.UpdateOrganization;
using Management.Application.Features.Organizations.Queries.Organizations.GetOrganization.GetOrganizationById;
using Management.Application.Features.Organizations.Queries.Organizations.GetOrganizationsList;
using Management.Application.Models.Organizations;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationController : CustomBaseController
    {
        private readonly ILogger<OrganizationController> _logger;

        public OrganizationController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<OrganizationController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }


        [HttpGet(Name = "GetOrganizationList")]
        [ProducesResponseType(typeof(IEnumerable<OrganizationViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<OrganizationViewModel>>> GetOrganizationList()
        {
            var query = new GetOrganizationsListQuery();
            var organizations = await _mediator.Send(query);
            return Ok(organizations);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(OrganizationViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<OrganizationViewModel>> GetOrganizationById(int id)
        {

            var query = new GetOrganizationByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"Organization with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddOrganization(AddOrganizationCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateOrganization(UpdateOrganizationCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteOrganization(DeleteOrganizationCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
    }
}
