﻿using Management.Application.Features.Definitions.Commands.Permissions.AddPermission;
using Management.Application.Features.Definitions.Commands.Permissions.DeletePermission;
using Management.Application.Features.Definitions.Commands.Permissions.UpdatePermission;
using Management.Application.Features.Definitions.Queries.Permissions.GetPermission.GetPermissionById;
using Management.Application.Features.Definitions.Queries.Permissions.GetPermissionsList.GetPermissionsListByAndIsDisplay;
using Management.Application.Models.Authentications;
using Management.Application.Models.Commons;
using Management.Application.Models.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionController : CustomBaseController
    {
        private readonly ILogger<PermissionController> _logger;

        public PermissionController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<PermissionController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PermissionViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<PermissionViewModel>>> GetPermissionsListByIsDisplay(bool IsDisplay)
        {
            var query = new GetPermissionsListByIsDisplayQuery(IsDisplay);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(PermissionViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<PermissionViewModel>> GetPermissionById(int id)
        {

            var query = new GetPermissionByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"Permission with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<PermissionViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<PermissionViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<PermissionViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<PermissionViewModel>().Count();

        //    var itemsOnPage = await (new List<PermissionViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<PermissionViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddPermission(AddPermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdatePermission(UpdatePermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeletePermission(DeletePermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
