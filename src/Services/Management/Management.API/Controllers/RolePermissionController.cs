﻿using Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission;
using Management.Application.Features.Organizations.Commands.RolePermissions.DeleteRolePermission;
using Management.Application.Features.Organizations.Commands.RolePermissions.UpdateRolePermission;
using Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermission.GetRolePermissionById;
using Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermissionsList.GetRolePermissionsListByOrganizationIdAndIsDisplay;
using Management.Application.Models.Authentications;
using Management.Application.Models.Commons;
using Management.Application.Models.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RolePermissionController : CustomBaseController
    {
        private readonly ILogger<RolePermissionController> _logger;

        public RolePermissionController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<RolePermissionController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RolePermissionViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<RolePermissionViewModel>>> GetRolePermissionsListByUserId(string roleId)
        {
            var query = new GetRolePermissionsListByRoleIdQuery(roleId);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(RolePermissionViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<RolePermissionViewModel>> GetRolePermissionById(int id)
        {

            var query = new GetRolePermissionByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"RolePermission with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<RolePermissionViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<RolePermissionViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<RolePermissionViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<RolePermissionViewModel>().Count();

        //    var itemsOnPage = await (new List<RolePermissionViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<RolePermissionViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddRolePermission(AddRolePermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateRolePermission(UpdateRolePermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteRolePermission(DeleteRolePermissionCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
