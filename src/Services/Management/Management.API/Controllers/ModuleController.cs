﻿using Management.Application.Features.Definitions.Commands.Modules.AddModule;
using Management.Application.Features.Definitions.Commands.Modules.DeleteModule;
using Management.Application.Features.Definitions.Commands.Modules.UpdateModule;
using Management.Application.Features.Definitions.Queries.Modules.Get.GetModuleById;
using Management.Application.Features.Definitions.Queries.Modules.GetList;
using Management.Application.Models.Definitions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    [EnableCors("MyPolicy")]
    public class ModuleController : CustomBaseController
    {
        private readonly ILogger<ModuleController> _logger;

        public ModuleController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<ModuleController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }

        [HttpGet]
        //[ProducesResponseType(typeof(IEnumerable<ModuleViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleViewModel>>> GetAllModulesList()
        {
            var query = new GetAllModulesListQuery();
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{filterText}")]
        [ProducesResponseType(typeof(IEnumerable<ModuleViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleViewModel>>> GetModulesListByFilterText(string filterText)
        {
            var query = new GetModulesListByFilterTextQuery(filterText);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{isDisplay}")]
        [ProducesResponseType(typeof(IEnumerable<ModuleViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ModuleViewModel>>> GetModulesListByIsDisplay(bool IsDisplay)
        {
            var query = new GetModulesListByIsDisplayQuery(IsDisplay);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ModuleViewModel), (int)HttpStatusCode.OK)]
        //[PermissionAuthorizeFilter(permissionName: PermissionProvider.ModuleGetName)]
        public async Task<ActionResult<ModuleViewModel>> GetModuleById(int id)
        {

            var query = new GetModuleByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"Module with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<ModuleViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<ModuleViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<ModuleViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<ModuleViewModel>().Count();

        //    var itemsOnPage = await (new List<ModuleViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<ModuleViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddModule(AddModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateModule(UpdateModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteModule(DeleteModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
