﻿using Management.API.Handlers;
using Management.Application.Features.Authentications.Commands.Users.AddUser;
using Management.Application.Features.Authentications.Queries.Users.GetUser;
using Management.Application.Features.Authentications.Queries.Users.GetUsersList.GetUserListByOrganizationId;
using Management.Application.Models.Authentications;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        IConfiguration _configuration;

        public UserController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> Login(string email, string password)
        {
            TokenHandler._configuration = _configuration;

            var query = new GetUserByEmailAndPasswordQuery(email, password);
            var userViewModel = await _mediator.Send(query);



            return Ok(userViewModel != null ? TokenHandler.CreateAccessToken(userViewModel) : new UnauthorizedResult());
        }

        [HttpGet("{organizationId}", Name = "GetUser")]
        [ProducesResponseType(typeof(IEnumerable<UserViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> GetOrdersByUserName(int organizationId)
        {
            var query = new GetUserListByOrganizationIdQuery(organizationId);
            var users = await _mediator.Send(query);
            return Ok(users);
        }

        [HttpPost(Name = "AddUser")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddUser([FromBody] AddUserCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
    }
}
