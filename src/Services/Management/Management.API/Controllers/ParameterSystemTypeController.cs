﻿using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.AddParameter;
using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.DeleteParameter;
using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.UpdateParameter;
using Management.Application.Features.Parameters.Queries.ParameterSystemTypes.Get.GetParameterSystemTypeById;
using Management.Application.Models.Parameters;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ParameterSystemTypeController : CustomBaseController
    {
        private readonly ILogger<ParameterSystemTypeController> _logger;

        public ParameterSystemTypeController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<ParameterSystemTypeController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        //[HttpGet]
        //[ProducesResponseType(typeof(IEnumerable<ParameterSystemTypeViewModel>), (int)HttpStatusCode.OK)]
        //public async Task<ActionResult<IEnumerable<ParameterSystemTypeViewModel>>> GetParameterSystemTypesListByOrganizationIdAndIsDisplay(int organizationId, bool IsDisplay)
        //{
        //    var query = new GetParameterSystemTypesListByOrganizationIdAndIsDisplayQuery(organizationId, IsDisplay);
        //    var roles = await _mediator.Send(query);
        //    return Ok(roles);
        //}

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ParameterSystemTypeViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ParameterSystemTypeViewModel>> GetParameterSystemTypeById(int id)
        {

            var query = new GetParameterSystemTypeByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"ParameterSystemType with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<ParameterSystemTypeViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<ParameterSystemTypeViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<ParameterSystemTypeViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<ParameterSystemTypeViewModel>().Count();

        //    var itemsOnPage = await (new List<ParameterSystemTypeViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<ParameterSystemTypeViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddParameterSystemType(AddParameterSystemTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateParameterSystemType(UpdateParameterSystemTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteParameterSystemType(DeleteParameterSystemTypeCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
