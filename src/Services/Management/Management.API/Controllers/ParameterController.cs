﻿using Management.Application.Features.Parameters.Commands.Parameters.AddParameter;
using Management.Application.Features.Parameters.Commands.Parameters.DeleteParameter;
using Management.Application.Features.Parameters.Commands.Parameters.UpdateParameter;
using Management.Application.Features.Parameters.Queries.Parameters.Get.GetParameterById;
using Management.Application.Features.Parameters.Queries.Parameters.GetList.GetListByOrganizationIdAndIsDisplay;
using Management.Application.Models.Commons;
using Management.Application.Models.Organizations;
using Management.Application.Models.Parameters;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ParameterController : CustomBaseController
    {
        private readonly ILogger<ParameterController> _logger;

        public ParameterController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<ParameterController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ParameterViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ParameterViewModel>>> GetParametersListByOrganizationId(int organizationId)
        {
            var query = new GetParametersListByOrganizationIdQuery(organizationId);
            var roles = await _mediator.Send(query);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ParameterViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ParameterViewModel>> GetParameterById(int id)
        {

            var query = new GetParameterByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"Parameter with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        //[HttpGet]
        //[Route("items")]
        //[ProducesResponseType(typeof(PaginatedItemsViewModel<ParameterViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(IEnumerable<ParameterViewModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, int ids = null)
        //{
        //    if (!string.IsNullOrEmpty(ids))
        //    {
        //        var items = new List<ParameterViewModel>();

        //        if (!items.Any())
        //        {
        //            return BadRequest("ids value invalid. Must be comma-separated list of numbers");
        //        }

        //        return Ok(items);
        //    }

        //    var totalItems = new List<ParameterViewModel>().Count();

        //    var itemsOnPage = await (new List<ParameterViewModel>())
        //        .OrderBy(c => c.Name)
        //        .Skip(pageSize * pageIndex)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    /* The "awesome" fix for testing Devspaces */

        //    /*
        //    foreach (var pr in itemsOnPage) {
        //        pr.Name = "Awesome " + pr.Name;
        //    }

        //    */



        //    var model = new PaginatedItemsViewModel<ParameterViewModel>(pageIndex, pageSize, totalItems, itemsOnPage);

        //    return Ok(model);
        //}

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddParameter(AddParameterCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateParameter(UpdateParameterCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteParameter(DeleteParameterCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
