﻿
using Management.Application.Features.Organizations.Commands.OrganizationModules.AddOrganizationModule;
using Management.Application.Features.Organizations.Commands.OrganizationModules.DeleteOrganizationModule;
using Management.Application.Features.Organizations.Commands.OrganizationModules.UpdateOrganizationModule;
using Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModule.GetOrganizationModuleById;
using Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModulesList;
using Management.Application.Models.Organizations;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.ControllerBases;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationModuleController : CustomBaseController
    {
        private readonly ILogger<OrganizationModuleController> _logger;

        public OrganizationModuleController(IMediator mediator,
            IConfiguration configuration,
            ISharedIdentityService sharedIdentityService,
            ILogger<OrganizationModuleController> logger,
            IMemoryCache memoryCache) : base(mediator, configuration, sharedIdentityService, memoryCache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("[action]/{organizationId}")]
        [ProducesResponseType(typeof(IEnumerable<OrganizationModuleViewModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<OrganizationModuleViewModel>>> GetOrganizationModuleByOrganizationIdList(int organizationId)
        {
            var query = new GetOrganizationModulesListByOrganizationIdQuery(organizationId);
            var organizations = await _mediator.Send(query);
            return Ok(organizations);
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(OrganizationModuleViewModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<OrganizationModuleViewModel>> GetOrganizationModuleById(int id)
        {

            var query = new GetOrganizationModuleByIdQuery(id);
            var role = await _mediator.Send(query);

            if (role == null)
            {
                _logger.LogError($"OrganizationModule with id: {id}, not found.");
                return NotFound();
            }

            return Ok(role);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> AddOrganizationModule(AddOrganizationModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> UpdateOrganizationModule(UpdateOrganizationModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> DeleteOrganizationModule(DeleteOrganizationModuleCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
    }
}
