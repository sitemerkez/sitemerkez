using Management.API.Extentions;
using Management.Infrastructure.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Management.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                   .Build()
                   .MigrateDatabase<ApplicationContext>((context, services) =>
                   {
                       //ApplicationContextSeed
                       // .SeedAsync(context)
                       // .Wait();
                   })
                   .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
