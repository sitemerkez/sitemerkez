using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Management.Application;
using Management.Infrastructure;
using Management.Infrastructure.Persistence;
using Microsoft.OpenApi.Models;
using Shared.Services;
using Management.Infrastructure.Datas;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Management.API
{
    public class Startup

    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationServices();
            services.AddInfrastructureServices(_configuration);
            services.AddHttpContextAccessor();

            services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationContext>((serviceProvider, optionsBuilder) =>
            {
                optionsBuilder.UseNpgsql(_configuration.GetConnectionString("DefaultConnection"));
                optionsBuilder.UseInternalServiceProvider(serviceProvider);
            });

            services.AddDatabaseDeveloperPageExceptionFilter();

            #region JWTToken

            SymmetricSecurityKey signInKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            string authenticationProviderKey = _configuration["JWT:Secret"];
            services.AddAuthentication(option => option.DefaultAuthenticateScheme = authenticationProviderKey)
                .AddJwtBearer(authenticationProviderKey, options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = signInKey,
                        ValidateIssuer = true,
                        ValidIssuer = _configuration["JWT:Issuer"],
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        RequireExpirationTime = true
                    };
                });

            #endregion

            services.AddMemoryCache();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Management.API", Version = "v1" });
            });

            #region Shared

            services.AddScoped<ISharedIdentityService, SharedIdentityService>();
            services.AddScoped<IEncryptionService, EncryptionService>();

            #endregion

            services.AddTransient<ApplicationContext>();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [Obsolete]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Management.API v1"));
                DataSeeding.Seed(app);  //Uygulama geliştirme aşamasında çağırıyoruz. 
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
           
            app.UseCors("MyPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
