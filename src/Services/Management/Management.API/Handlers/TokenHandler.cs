﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Security.Claims;
using Management.Application.Models.Authentications;
using Shared.Services;

namespace Management.API.Handlers
{
    public static class TokenHandler
    {
        public static IConfiguration _configuration;
        public static dynamic CreateAccessToken(UserViewModel userViewModel)
        {
            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:Security"]));
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = symmetricSecurityKey,
                ValidateIssuer = true,
                ValidIssuer = _configuration["JWT:Issuer"],
                ValidateAudience = true,
                ValidAudience = _configuration["JWT:Audience"],
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true
            };
            DateTime now = DateTime.UtcNow;

            var claims = new List<Claim>();

            claims.Add(new Claim("userId", userViewModel.Id.ToString()));
            claims.Add(new Claim("organizationId", userViewModel.OrganizationId.ToString()));
            claims.Add(new Claim("organizationName", userViewModel.OrganizationName));

            // Add roles as multiple claims
            foreach (var role in userViewModel.RoleIds)
            {
                claims.Add(new Claim("roleIds", role));
            }

            claims.Add(new Claim("IsDisplay", userViewModel.IsDisplay.ToString()));

            JwtSecurityToken jwt = new JwtSecurityToken(
                     issuer: _configuration["JWT:Issuer"],
                     audience: _configuration["JWT:Audience"],
                     claims: claims,
                     notBefore: now,
                     expires: now.Add(TimeSpan.FromDays(1)),
                     signingCredentials: new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256)
                 );
            return new
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwt),
                Expires = TimeSpan.FromMinutes(2).TotalSeconds
            };
        }
    }
}
