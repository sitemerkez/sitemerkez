﻿
using Management.Domain.Entities.Concrete.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Services.Commons
{
    public partial interface IBaseService<TEntity> where TEntity : BaseEntity
    {
        #region Insert

        Task<TEntity> AddEntityAsync(TEntity entity);

        void AddEntitiesAsync(List<TEntity> entities);

        #endregion

        #region Select

        Task<TEntity> GetEntityById(int entityId);

        Task<IEnumerable<TEntity>> GetEntitiesByIds(int[] entityIds);

        IPagedList<TEntity> GetPagedEntities(Expression<Func<TEntity, bool>> predicate = null, string includeString = null, int pageIndex = 0, int pageSize = int.MaxValue, Sort sort = null);

        Task<int> CountEntitiesAsync();

        #endregion

        #region Delete

        void DeleteEntityAsync(TEntity entity);

        /// <summary>
        void DeleteEntitiesAsync(IList<TEntity> entities);

        #endregion

        #region Update

        void UpdateEntityAsync(TEntity entity);

        void UpdateEntitiesAsync(List<TEntity> entities);

        #region Sort

        //void DownEntity(TEntity entity);

        //void UpEntity(TEntity entity);

        #endregion

        #endregion
    }
}
