﻿using Management.Domain.Entities.Concrete.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Services.Parameters
{
    public interface IParameterSystemTypeService
    {
        #region Insert

        Task<ParameterSystemType> AddEntityAsync(ParameterSystemType entity);

        #endregion

        #region Select

        Task<ParameterSystemType> GetEntityByIdAsync(int id);

        Task<IEnumerable<ParameterSystemType>> GetParameterSystemTypesListByOrganizationId(int organizationId);

        Task<IEnumerable<ParameterSystemType>> GetParameterSystemTypesListByOrganizationIdAndSystemTypeId(int organizationId, int systemTypeId);

        #endregion

        #region Update

        Task UpdateEntityAsync(ParameterSystemType entity);

        #endregion

        #region Delete

        Task DeleteEntityAsync(int id);
        
        #endregion

    }
}
