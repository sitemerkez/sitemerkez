﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Application.Services.Commons;
using Management.Domain.Entities.Concrete.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Services.Parameters
{
    public interface IParameterService : IBaseService<Parameter>
    {
        
        #region Select

        Task<IEnumerable<Parameter>> GetParametersListByOrganizationId(int organizationId);

        #endregion

    }
}
