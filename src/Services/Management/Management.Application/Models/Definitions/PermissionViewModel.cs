﻿using Management.Domain.Entities.Concrete.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Definitions
{
	public class PermissionViewModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int OperationTypeId { get; set; }

		public string OperationTypeName { get; set; }

		public string ParentPermissionName { get; set; }
	}
}
