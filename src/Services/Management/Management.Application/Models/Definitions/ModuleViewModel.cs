﻿using Management.Domain.Entities.Concrete.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Definitions
{
	public class ModuleViewModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public int ModuleTypeId { get; set; }

		public string ModuleTypeName { get; set; }

		public bool IsDisplay { get; set; }

		public DateTime CreatedOn { get; set; }
	}
}
