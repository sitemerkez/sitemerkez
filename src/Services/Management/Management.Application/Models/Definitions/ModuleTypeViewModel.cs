﻿using Management.Domain.Entities.Concrete.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Definitions
{
	public class ModuleTypeViewModel
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}
}
