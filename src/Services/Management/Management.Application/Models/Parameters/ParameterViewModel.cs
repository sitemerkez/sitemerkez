﻿using Management.Application.Models.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Parameters
{
    public class ParameterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DataTypeId { get; set; }
        public string DataTypeName { get; set; }
        public string Description { get; set; }
        public int Maximum { get; set; }
        public int DisplayOrder { get; set; }
    }
}
