﻿using Management.Application.Models.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Parameters
{
    public class ParameterSystemTypeViewModel
    {
        public int Id { get; set; }
        public string ParameterId { get; set; }
        public string ParameterName { get; set; }
        public int ParameterTypeId { get; set; }
        public string ParameterTypeName { get; set; }
    }
}
