﻿using Management.Domain.Entities.Concrete.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Organizations
{
	public class RoleViewModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int RoleTypeId { get; set; }

		public string RoleTypeName { get; set; }

		public bool Selected { get; set; }
	}
}
