﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Organizations
{
    public class OrganizationModuleViewModel
    {
        public int OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        public string ModuleId { get; set; }

        public string ModuleName { get; set; }

        public DateTime BaslangicTarih { get; set; }
        public DateTime? BitisTarih { get; set; }

    }
}
