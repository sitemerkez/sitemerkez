﻿using Management.Application.Models.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Authentications
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            RoleIds = new List<string>();
        }
        public int Id { get; set; }
        public string UserName
        {
            get
            {
                return Email;
            }
        }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsDisplay { get; set; }
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public List<string> RoleIds { get; set; }


    }
}
