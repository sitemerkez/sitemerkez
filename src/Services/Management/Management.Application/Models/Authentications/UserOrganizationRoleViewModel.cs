﻿using Management.Application.Models.Definitions;
using Management.Application.Models.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Models.Authentications
{
    public class UserOrganizationRoleViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
