﻿using AutoMapper;
using Management.Application.Features.Authentications.Commands.Users.AddUser;
using Management.Application.Features.Definitions.Commands.Modules.AddModule;
using Management.Application.Features.Definitions.Commands.Modules.UpdateModule;
using Management.Application.Features.Definitions.Commands.Permissions.AddPermission;
using Management.Application.Features.Definitions.Commands.Permissions.UpdatePermission;
using Management.Application.Features.Organizations.Commands.OrganizationModules.AddOrganizationModule;
using Management.Application.Features.Organizations.Commands.OrganizationModules.UpdateOrganizationModule;
using Management.Application.Features.Organizations.Commands.Organizations.AddOrganization;
using Management.Application.Features.Organizations.Commands.Organizations.UpdateOrganization;
using Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission;
using Management.Application.Features.Organizations.Commands.RolePermissions.UpdateRolePermission;
using Management.Application.Features.Parameters.Commands.Parameters.AddParameter;
using Management.Application.Features.Parameters.Commands.Parameters.UpdateParameter;
using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.AddParameter;
using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.UpdateParameter;
using Management.Application.Models.Authentications;
using Management.Application.Models.Definitions;
using Management.Application.Models.Organizations;
using Management.Application.Models.Parameters;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using Management.Domain.Entities.Concrete.Organizations;
using Management.Domain.Entities.Concrete.Parameters;

namespace Management.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Organization, OrganizationViewModel>().ReverseMap();
            CreateMap<Organization, AddOrganizationCommand>().ReverseMap();
            CreateMap<Organization, UpdateOrganizationCommand>().ReverseMap();

          

            //CreateMap<User, UserViewModel>().ReverseMap();
            //CreateMap<User, AddUserCommand>().ReverseMap();

            //CreateMap<UserOrganizationRole, UserOrganizationRoleViewModel>().ReverseMap();
            //CreateMap<UserOrganizationRole, AddUserOrganizationRoleCommand>().ReverseMap();

            CreateMap<ModuleType, ModuleTypeViewModel>().ReverseMap();
            CreateMap<ModuleType, AddModuleTypeCommand>().ReverseMap();
            CreateMap<ModuleType, UpdateModuleTypeCommand>().ReverseMap();

            CreateMap<Module, ModuleViewModel>().ReverseMap();
            CreateMap<Module, AddModuleCommand>().ReverseMap();
            CreateMap<Module, UpdateModuleCommand>().ReverseMap();

            //CreateMap<OrganizationModule, OrganizationModuleViewModel>()
            //     .ForMember(dest => dest.ModuleName, mo => mo.MapFrom(src => src.Module.Name))
            //     .ReverseMap();
            //CreateMap<OrganizationModule, AddOrganizationModuleCommand>().ReverseMap();
            //CreateMap<OrganizationModule, UpdateOrganizationModuleCommand>().ReverseMap();

            CreateMap<Permission, PermissionViewModel>()
                .ForMember(dest => dest.OperationTypeName, mo => mo.MapFrom(src => src.OperationType.Name))
                .ReverseMap();
            CreateMap<Permission, AddPermissionCommand>().ReverseMap();
            CreateMap<Permission, UpdatePermissionCommand>().ReverseMap();

            CreateMap<RolePermission, RolePermissionViewModel>().ReverseMap();
            CreateMap<RolePermission, AddRolePermissionCommand>().ReverseMap();
            CreateMap<RolePermission, UpdateRolePermissionCommand>().ReverseMap();

            CreateMap<Parameter, ParameterViewModel>().ReverseMap();
            CreateMap<Parameter, AddParameterCommand>().ReverseMap();
            CreateMap<Parameter, UpdateParameterCommand>().ReverseMap();

            CreateMap<ParameterSystemType, ParameterSystemTypeViewModel>()
                .ForMember(dest => dest.ParameterTypeName, mo => mo.MapFrom(src => src.SystemType.Name))
                .ReverseMap();
            CreateMap<ParameterSystemType, AddParameterSystemTypeCommand>().ReverseMap();
            CreateMap<ParameterSystemType, UpdateParameterSystemTypeCommand>().ReverseMap();


        }
    }
}
