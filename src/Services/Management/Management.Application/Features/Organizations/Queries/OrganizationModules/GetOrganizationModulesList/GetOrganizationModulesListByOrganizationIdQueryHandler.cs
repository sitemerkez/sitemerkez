﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModulesList
{
    //public class GetOrganizationModulesListByOrganizationIdQueryHandler : IRequestHandler<GetOrganizationModulesListByOrganizationIdQuery, List<OrganizationModuleViewModel>>
    //{
    //    private readonly IOrganizationModuleRepository _organizationModuleRepository;
    //    private readonly IModuleRepository _moduleRepository;
    //    private readonly IMapper _mapper;

    //    public GetOrganizationModulesListByOrganizationIdQueryHandler(IOrganizationModuleRepository OrganizationModuleRepository,
    //        IModuleRepository moduleRepository,
    //        IMapper mapper)
    //    {
    //        _organizationModuleRepository = OrganizationModuleRepository ?? throw new ArgumentNullException(nameof(OrganizationModuleRepository));
    //        _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));

    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //    }

    //    public async Task<List<OrganizationModuleViewModel>> Handle(GetOrganizationModulesListByOrganizationIdQuery request, CancellationToken cancellationToken)
    //    {
    //        var OrganizationModulesList = await _organizationModuleRepository.GetOrganizationModulesListByOrganizationId(request.OrganizationId);

    //        var modules = await _moduleRepository.GetAllEntitiesAsync();

    //        foreach(var item in OrganizationModulesList)
    //        {
    //            item.Module = modules.Where(p => p.Id == item.ModuleId).FirstOrDefault();
    //        }

    //        return _mapper.Map<List<OrganizationModuleViewModel>>(OrganizationModulesList);
    //    }
    //}
}
