﻿using Management.Application.Models.Authentications;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModulesList
{
    public class GetOrganizationModulesListByOrganizationIdQuery : IRequest<List<OrganizationModuleViewModel>>
    {
        public int OrganizationId { get; set; }

        public GetOrganizationModulesListByOrganizationIdQuery(int organizationId)
        {
            OrganizationId = organizationId;
        }
    }
}
