﻿using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModule.GetOrganizationModuleById
{
    public class GetOrganizationModuleByIdQuery : IRequest<OrganizationModuleViewModel>
    {
        public int Id { get; set; }

        public GetOrganizationModuleByIdQuery(int id)
        {
            Id = id;
        }
    }
}
