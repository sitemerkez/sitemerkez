﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.OrganizationModules.GetOrganizationModule.GetOrganizationModuleById
{
    //public class GetOrganizationModuleByIdQueryHandler : IRequestHandler<GetOrganizationModuleByIdQuery, OrganizationModuleViewModel>
    //{
    //    private readonly IOrganizationModuleRepository _organizationModuleRepository;
    //    private readonly IModuleRepository _moduleRepository;
    //    private readonly IMapper _mapper;

    //    public GetOrganizationModuleByIdQueryHandler(IOrganizationModuleRepository organizationModuleRepository,
    //        IModuleRepository moduleRepository, IMapper mapper)
    //    {
    //        _organizationModuleRepository = organizationModuleRepository ?? throw new ArgumentNullException(nameof(organizationModuleRepository));
    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //        _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
    //    }

    //    public async Task<OrganizationModuleViewModel> Handle(GetOrganizationModuleByIdQuery request, CancellationToken cancellationToken)
    //    {
    //        var modules = await _moduleRepository.GetAllEntitiesAsync();

    //        var organizationModuleEntity = await _organizationModuleRepository.GetEntityByIdAsync(request.Id);

    //        organizationModuleEntity.Module = modules.Where(p => p.Id == organizationModuleEntity.ModuleId).FirstOrDefault();

    //        return _mapper.Map<OrganizationModuleViewModel>(organizationModuleEntity);
    //    }
    //}
}
