﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Authentications;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermission.GetRolePermissionById
{
    public class GetRolePermissionByIdQueryHandler : IRequestHandler<GetRolePermissionByIdQuery, RolePermissionViewModel>
    {
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly IMapper _mapper;

        public GetRolePermissionByIdQueryHandler(IRolePermissionRepository rolePermissionRepository, IMapper mapper)
        {
            _rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<RolePermissionViewModel> Handle(GetRolePermissionByIdQuery request, CancellationToken cancellationToken)
        {
            var rolePermissionEntity = await _rolePermissionRepository.GetEntityByIdAsync(request.Id);
            return _mapper.Map<RolePermissionViewModel>(rolePermissionEntity);
        }
    }
}
