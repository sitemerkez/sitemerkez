﻿using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermission.GetRolePermissionById
{
    
  public class GetRolePermissionByIdQuery : IRequest<RolePermissionViewModel>
    {
        public int Id { get; set; }
  
        public GetRolePermissionByIdQuery(int id)
        {
            Id = id;
        }
    }
}
