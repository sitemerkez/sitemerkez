﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Authentications;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermissionsList.GetRolePermissionsListByOrganizationIdAndIsDisplay
{
    public class GetRolePermissionsListByRoleIdQueryHandler : IRequestHandler<GetRolePermissionsListByRoleIdQuery, List<RolePermissionViewModel>>
    {
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly IMapper _mapper;

        public GetRolePermissionsListByRoleIdQueryHandler(IRolePermissionRepository rolePermissionRepository, IMapper mapper)
        {
            _rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<RolePermissionViewModel>> Handle(GetRolePermissionsListByRoleIdQuery request, CancellationToken cancellationToken)
        {
            var rolePermissionsList = await _rolePermissionRepository.GetEntitiesAsync(x => x.RoleId == request.RoleId);
            return _mapper.Map<List<RolePermissionViewModel>>(rolePermissionsList);
        }
    }
}
