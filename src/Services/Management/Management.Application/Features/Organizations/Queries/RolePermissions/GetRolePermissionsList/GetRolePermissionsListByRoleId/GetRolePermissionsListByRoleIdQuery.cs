﻿using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermissionsList.GetRolePermissionsListByOrganizationIdAndIsDisplay
{
    public class GetRolePermissionsListByRoleIdQuery : IRequest<List<RolePermissionViewModel>>
    {
        public string RoleId { get; set; }

        public GetRolePermissionsListByRoleIdQuery(string roleId)
        {
            RoleId = roleId;
        }
    }
}
