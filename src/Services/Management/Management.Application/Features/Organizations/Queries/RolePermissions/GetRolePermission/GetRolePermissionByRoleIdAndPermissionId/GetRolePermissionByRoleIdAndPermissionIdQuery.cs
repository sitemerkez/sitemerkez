﻿using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.RolePermissions.GetRolePermission.GetRolePermissionById
{
    
  public class GetRolePermissionByRoleIdAndPermissionIdQuery : IRequest<RolePermissionViewModel>
    {
        public string RoleId { get; set; }
        public int PermissionId { get; set; }

        public GetRolePermissionByRoleIdAndPermissionIdQuery(string roleId, int permissionId)
        {
            RoleId = roleId;
            PermissionId = permissionId;
        }
    }
}
