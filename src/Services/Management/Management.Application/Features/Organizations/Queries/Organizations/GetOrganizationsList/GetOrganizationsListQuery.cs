﻿using Management.Application.Models.Authentications;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Organizations.Queries.Organizations.GetOrganizationsList
{
    public class GetOrganizationsListQuery : IRequest<List<OrganizationViewModel>>
    {
        public GetOrganizationsListQuery()
        {
        }
    }
}
