﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Models.Authentications;
using Management.Application.Models.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.Organizations.GetOrganizationsList
{
    public class GetOrganizationsListQueryHandler : IRequestHandler<GetOrganizationsListQuery, List<OrganizationViewModel>>
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IMapper _mapper;

        public GetOrganizationsListQueryHandler(IOrganizationRepository organizationRepository, IMapper mapper)
        {
            _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<OrganizationViewModel>> Handle(GetOrganizationsListQuery request, CancellationToken cancellationToken)
        {
            var OrganizationsList = await _organizationRepository.GetAllEntitiesAsync();
            return _mapper.Map<List<OrganizationViewModel>>(OrganizationsList);
        }
    }
}
