﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.Organizations.GetOrganization.GetOrganizationById
{
    public class GetOrganizationByIdQueryHandler : IRequestHandler<GetOrganizationByIdQuery, OrganizationViewModel>
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IMapper _mapper;

        public GetOrganizationByIdQueryHandler(IOrganizationRepository organizationRepository, IMapper mapper)
        {
            _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<OrganizationViewModel> Handle(GetOrganizationByIdQuery request, CancellationToken cancellationToken)
        {
            var organizationEntity = await _organizationRepository.GetEntityByIdAsync(request.Id);
            return _mapper.Map<OrganizationViewModel>(organizationEntity);
        }
    }
}
