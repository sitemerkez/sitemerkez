﻿using Management.Application.Models.Organizations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Queries.Organizations.GetOrganization.GetOrganizationById
{
    
  public class GetOrganizationByIdQuery : IRequest<OrganizationViewModel>
    {
        public int Id { get; set; }
  
        public GetOrganizationByIdQuery(int id)
        {
            Id = id;
        }
    }
}
