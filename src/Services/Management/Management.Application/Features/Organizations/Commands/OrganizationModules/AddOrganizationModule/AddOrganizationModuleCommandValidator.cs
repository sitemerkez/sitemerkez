﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Organizations.Commands.OrganizationModules.AddOrganizationModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModuless.AddOrganizationModule
{
    public class AddOrganizationModuleCommandValidator : EntityBaseAbstractValidator<AddOrganizationModuleCommand>
    {
        public AddOrganizationModuleCommandValidator()
        {
            RuleFor(p => p.ModuleId)
                .NotEmpty().WithMessage("{ModuleId} is required.");

            RuleFor(p => p.BaslangicTarih)
              .NotEmpty().WithMessage("{BaslangicTarih} is required.");

        }
    }
}
