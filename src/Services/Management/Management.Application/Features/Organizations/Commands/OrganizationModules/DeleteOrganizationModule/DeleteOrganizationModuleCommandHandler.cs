﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Exceptions;
using Management.Application.Features.Organizations.Commands.OrganizationModules.DeleteOrganizationModule;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.DeleteOrganizationModule
{
    //public class DeleteOrganizationModuleCommandHandler : IRequestHandler<DeleteOrganizationModuleCommand, bool>
    //{
    //    private readonly IOrganizationModuleRepository _organizationModuleRepository;
    //    private readonly IMapper _mapper;
    //    private readonly ILogger<DeleteOrganizationModuleCommandHandler> _logger;

    //    public DeleteOrganizationModuleCommandHandler(IOrganizationModuleRepository organizationRepository, IMapper mapper, ILogger<DeleteOrganizationModuleCommandHandler> logger)
    //    {
    //        _organizationModuleRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    //    }

    //    public async Task<bool> Handle(DeleteOrganizationModuleCommand request, CancellationToken cancellationToken)
    //    {
    //        var organizationToDelete = await _organizationModuleRepository.GetEntityByIdAsync(request.Id);
    //        if (organizationToDelete == null)
    //        {
    //            throw new NotFoundException(nameof(Organization), request.Id);
    //        }

    //        await _organizationModuleRepository.DeleteEntityAsync(organizationToDelete);
    //        _logger.LogInformation($"Organization {organizationToDelete.Id} is successfully deleted.");

    //        return true;
    //    }
    //}
}
