﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.AddOrganizationModule
{
    //public class AddOrganizationModuleCommandHandler : IRequestHandler<AddOrganizationModuleCommand, bool>
    //{
    //    private readonly IOrganizationModuleRepository _organizationRepository;
    //    private readonly IMapper _mapper;
    //    private readonly ILogger<AddOrganizationModuleCommandHandler> _logger;

    //    public AddOrganizationModuleCommandHandler(IOrganizationModuleRepository organizationRepository, IMapper mapper, ILogger<AddOrganizationModuleCommandHandler> logger)
    //    {
    //        _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    //    }

    //    public async Task<bool> Handle(AddOrganizationModuleCommand request, CancellationToken cancellationToken)
    //    {
    //        var organizationEntity = _mapper.Map<OrganizationModule>(request);
    //        var newOrganizationModule = await _organizationRepository.AddEntityAsync(organizationEntity);

    //        _logger.LogInformation($"OrganizationModule {newOrganizationModule.Id} is successfully created.");


    //        return true;
    //    }
    //}
}
