﻿using Management.Application.Features.Commons;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.UpdateOrganizationModule
{
    public class UpdateOrganizationModuleCommand : IOrganizationEntityBaseWithIdRequest
    {
        public string ModuleId { get; set; }

        public DateTime BaslangicTarih { get; set; }

        public DateTime? BitisTarih { get; set; }
    }
}
