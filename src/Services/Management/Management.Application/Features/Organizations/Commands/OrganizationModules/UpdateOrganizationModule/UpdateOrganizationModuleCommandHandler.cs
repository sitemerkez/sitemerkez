﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.UpdateOrganizationModule
{
    //public class UpdateOrganizationModuleCommandHandler : IRequestHandler<UpdateOrganizationModuleCommand, bool>
    //{
    //    private readonly IOrganizationModuleRepository _organizationModuleRepository;
    //    private readonly IMapper _mapper;
    //    private readonly ILogger<UpdateOrganizationModuleCommandHandler> _logger;

    //    public UpdateOrganizationModuleCommandHandler(IOrganizationModuleRepository organizationRepository, IMapper mapper, ILogger<UpdateOrganizationModuleCommandHandler> logger)
    //    {
    //        _organizationModuleRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    //    }

    //    public async Task<bool> Handle(UpdateOrganizationModuleCommand request, CancellationToken cancellationToken)
    //    {
    //        var organizationToUpdate = await _organizationModuleRepository.GetEntityByIdAsync(request.Id);
    //        if (organizationToUpdate == null)
    //        {
    //            throw new NotFoundException(nameof(OrganizationModule), request.Id);
    //        }

    //        _mapper.Map(request, organizationToUpdate, typeof(UpdateOrganizationModuleCommand), typeof(OrganizationModule));

    //        await _organizationModuleRepository.UpdateEntityAsync(organizationToUpdate);

    //        _logger.LogInformation($"OrganizationModule {organizationToUpdate.Id} is successfully updated.");

    //        return true;
    //    }
    //}
}
