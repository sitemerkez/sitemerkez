﻿using FluentValidation;
using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.UpdateOrganizationModule
{
    public class UpdateOrganizationModuleCommandValidator : OrganizationEntityBaseAbstractValidator<UpdateOrganizationModuleCommand>
    {
        public UpdateOrganizationModuleCommandValidator()
        {
            RuleFor(p => p.ModuleId)
                .NotEmpty().WithMessage("{Name} is required.");

            RuleFor(p => p.BaslangicTarih)
                .NotEmpty().WithMessage("{BaslangicTarih} is required.");


        }
    }
}
