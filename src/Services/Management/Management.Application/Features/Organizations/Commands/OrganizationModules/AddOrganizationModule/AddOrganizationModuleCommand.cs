﻿using Management.Application.Features.Commons;
using MediatR;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.OrganizationModules.AddOrganizationModule
{
    public class AddOrganizationModuleCommand : IOrganizationEntityBaseRequest
    {
        public string ModuleId { get; set; }

        public DateTime BaslangicTarih { get; set; }

        public DateTime? BitisTarih { get; set; }

    }
}
