﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.UpdateRolePermission
{
    public class UpdateRolePermissionCommand : IEntityBaseWithIdRequest
    {
        public string RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}
