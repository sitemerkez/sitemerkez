﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Authentications;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission
{
    public class AddRolePermissionCommandHandler : IRequestHandler<AddRolePermissionCommand, bool>
    {
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<AddRolePermissionCommandHandler> _logger;

        public AddRolePermissionCommandHandler(IRolePermissionRepository rolePermissionRepository, IMapper mapper, ILogger<AddRolePermissionCommandHandler> logger)
        {
            _rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddRolePermissionCommand request, CancellationToken cancellationToken)
        {
            var rolePermissionEntity = _mapper.Map<RolePermission>(request);
            var newRolePermission = await _rolePermissionRepository.AddEntityAsync(rolePermissionEntity);

            _logger.LogInformation($"RolePermission {newRolePermission.Id} is successfully created.");


            return true;
        }
    }
}
