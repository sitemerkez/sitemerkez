﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Authentications;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.UpdateRolePermission
{
    public class UpdateRolePermissionCommandHandler : IRequestHandler<UpdateRolePermissionCommand, bool>
    {
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateRolePermissionCommandHandler> _logger;

        public UpdateRolePermissionCommandHandler(IRolePermissionRepository rolePermissionRepository, IMapper mapper, ILogger<UpdateRolePermissionCommandHandler> logger)
        {
            _rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateRolePermissionCommand request, CancellationToken cancellationToken)
        {
            var rolePermissionToUpdate = await _rolePermissionRepository.GetEntityByIdAsync(request.Id);
            if (rolePermissionToUpdate == null)
            {
                throw new NotFoundException(nameof(RolePermission), request.Id);
            }

            _mapper.Map(request, rolePermissionToUpdate, typeof(UpdateRolePermissionCommand), typeof(RolePermission));

            await _rolePermissionRepository.UpdateEntityAsync(rolePermissionToUpdate);

            _logger.LogInformation($"RolePermission {rolePermissionToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
