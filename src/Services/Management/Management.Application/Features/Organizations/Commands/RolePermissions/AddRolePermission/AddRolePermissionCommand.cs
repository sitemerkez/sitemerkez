﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.Application.Features.Commons;
using MediatR;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission
{
    public class AddRolePermissionCommand : IEntityBaseRequest
    {
        public string RoleId { get; set; }
        public string PermissionId { get; set; }
    }
}
