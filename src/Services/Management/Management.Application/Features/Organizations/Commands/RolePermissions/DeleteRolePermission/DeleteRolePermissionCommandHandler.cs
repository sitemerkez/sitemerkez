﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Authentications;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.DeleteRolePermission
{
    public class DeleteRolePermissionCommandHandler : IRequestHandler<DeleteRolePermissionCommand, bool>
    {
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteRolePermissionCommandHandler> _logger;

        public DeleteRolePermissionCommandHandler(IRolePermissionRepository rolePermissionRepository, IMapper mapper, ILogger<DeleteRolePermissionCommandHandler> logger)
        {
            _rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteRolePermissionCommand request, CancellationToken cancellationToken)
        {
            var rolePermissionToDelete = await _rolePermissionRepository.GetEntityByIdAsync(request.Id);
            if (rolePermissionToDelete == null)
            {
                throw new NotFoundException(nameof(RolePermission), request.Id);
            }

            await _rolePermissionRepository.DeleteEntityAsync(rolePermissionToDelete);
            _logger.LogInformation($"RolePermission {rolePermissionToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
