﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.UpdateRolePermission
{
    public class UpdateRolePermissionCommandValidator : EntityBaseAbstractValidator<UpdateRolePermissionCommand>
    {
        public UpdateRolePermissionCommandValidator()
        {
            RuleFor(p => p.RoleId)
                .NotEmpty().WithMessage("{RoleId} is required.");

            RuleFor(p => p.PermissionId)
                .NotEmpty().WithMessage("{PermissionId} is required.");

        }
    }
}
