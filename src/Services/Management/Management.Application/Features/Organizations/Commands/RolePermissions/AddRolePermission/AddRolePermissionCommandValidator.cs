﻿using FluentValidation;
using Management.Application.Features.Commons;

namespace Management.Application.Features.Organizations.Commands.RolePermissions.AddRolePermission
{
    public class AddRolePermissionCommandValidator : EntityBaseAbstractValidator<AddRolePermissionCommand>
    {
        public AddRolePermissionCommandValidator()
        {
            RuleFor(p => p.RoleId)
                .NotEmpty().WithMessage("{Name} is required.");

            RuleFor(p => p.PermissionId)
               .NotEmpty().WithMessage("{PermissionId} is required.");

        }
    }
}
