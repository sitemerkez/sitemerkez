﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.UpdateOrganization
{
    public class UpdateOrganizationCommand : IEntityBaseWithIdRequest
    {
        public string Name { get; set; }
    }
}
