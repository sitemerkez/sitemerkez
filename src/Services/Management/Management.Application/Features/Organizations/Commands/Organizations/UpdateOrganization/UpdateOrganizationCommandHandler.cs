﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.UpdateOrganization
{
    public class UpdateOrganizationCommandHandler : IRequestHandler<UpdateOrganizationCommand, bool>
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateOrganizationCommandHandler> _logger;

        public UpdateOrganizationCommandHandler(IOrganizationRepository organizationRepository, IMapper mapper, ILogger<UpdateOrganizationCommandHandler> logger)
        {
            _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateOrganizationCommand request, CancellationToken cancellationToken)
        {
            var organizationToUpdate = await _organizationRepository.GetEntityByIdAsync(request.Id);
            if (organizationToUpdate == null)
            {
                throw new NotFoundException(nameof(Organization), request.Id);
            }

            _mapper.Map(request, organizationToUpdate, typeof(UpdateOrganizationCommand), typeof(Organization));

            await _organizationRepository.UpdateEntityAsync( organizationToUpdate);

            _logger.LogInformation($"Organization {organizationToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
