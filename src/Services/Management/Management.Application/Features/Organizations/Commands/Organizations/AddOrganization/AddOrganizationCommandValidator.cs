﻿using FluentValidation;
using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.AddOrganization
{
    public class AddOrganizationCommandValidator : EntityBaseAbstractValidator<AddOrganizationCommand>
    {
        public AddOrganizationCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(500).WithMessage("{Name} must not exceed 500 characters..");
        }
    }
}
