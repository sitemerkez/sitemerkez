﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.AddOrganization
{
    public class AddOrganizationCommandHandler : IRequestHandler<AddOrganizationCommand, bool>
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<AddOrganizationCommandHandler> _logger;

        public AddOrganizationCommandHandler(IOrganizationRepository organizationRepository, IMapper mapper, ILogger<AddOrganizationCommandHandler> logger)
        {
            _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
        {
            var organizationEntity = _mapper.Map<Organization>(request);
            var newOrganization = await _organizationRepository.AddEntityAsync(organizationEntity);

            _logger.LogInformation($"Organization {newOrganization.Id} is successfully created.");


            return true;
        }
    }
}
