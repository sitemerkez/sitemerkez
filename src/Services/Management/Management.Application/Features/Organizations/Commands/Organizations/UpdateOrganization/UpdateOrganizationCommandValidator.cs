﻿using FluentValidation;
using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.UpdateOrganization
{
    public class UpdateOrganizationCommandValidator : EntityBaseAbstractValidator<UpdateOrganizationCommand>
    {
        public UpdateOrganizationCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Name} must not exceed 50 characters..");

        }
    }
}
