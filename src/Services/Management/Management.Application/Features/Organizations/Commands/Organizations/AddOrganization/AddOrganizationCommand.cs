﻿using Management.Application.Features.Commons;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.AddOrganization
{
    public class AddOrganizationCommand : IEntityBaseRequest
    {
        public string Name { get; set; }
    }
}
