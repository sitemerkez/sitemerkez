﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Organizations;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Organizations.Commands.Organizations.DeleteOrganization
{
    public class DeleteOrganizationCommandHandler : IRequestHandler<DeleteOrganizationCommand, bool>
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteOrganizationCommandHandler> _logger;

        public DeleteOrganizationCommandHandler(IOrganizationRepository organizationRepository, IMapper mapper, ILogger<DeleteOrganizationCommandHandler> logger)
        {
            _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteOrganizationCommand request, CancellationToken cancellationToken)
        {
            var organizationToDelete = await _organizationRepository.GetEntityByIdAsync(request.Id);
            if (organizationToDelete == null)
            {
                throw new NotFoundException(nameof(Organization), request.Id);
            }

            await _organizationRepository.DeleteEntityAsync(organizationToDelete);
            _logger.LogInformation($"Organization {organizationToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
