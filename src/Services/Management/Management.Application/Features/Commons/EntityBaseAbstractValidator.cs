﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Commons
{
    public abstract class EntityBaseAbstractValidator<T> : AbstractValidator<T> where T : IEntityBaseRequest, new()
    {
        public EntityBaseAbstractValidator()
        {
            RuleFor(p => p.CreatedUserId)
              .NotNull().WithMessage("{CreatedUserId} is required.");

            RuleFor(p => p.CreatedDate)
             .NotNull().WithMessage("{CreatedDate} is required.");
             
            RuleFor(p => p.LastModifiedUserId)
                   .NotNull().WithMessage("{LastModifiedUserId} is required.");
        }
    }
}
