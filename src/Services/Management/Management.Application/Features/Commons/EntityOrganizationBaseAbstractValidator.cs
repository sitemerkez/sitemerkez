﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Commons
{
    public abstract class OrganizationEntityBaseAbstractValidator<T> : AbstractValidator<T> where T : IOrganizationEntityBaseRequest, new()
    {
        public OrganizationEntityBaseAbstractValidator()
        {
            RuleFor(p => p.OrganizationId)
             .NotNull().WithMessage("{OrganizationId} is required.");

            RuleFor(p => p.CreatedUserId)
              .NotNull().WithMessage("{CreatedUserId} is required.");

            RuleFor(p => p.CreatedDate)
             .NotNull().WithMessage("{CreatedDate} is required.");
             
            RuleFor(p => p.LastModifiedUserId)
                   .NotNull().WithMessage("{LastModifiedUserId} is required.");
        }
    }
}
