﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Commons
{
    public class IOrganizationEntityBaseWithIdRequest : IOrganizationEntityBaseRequest
    {
        public int Id { get; set; }
    }
}
