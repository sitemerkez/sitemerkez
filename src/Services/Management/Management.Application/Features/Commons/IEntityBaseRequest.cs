﻿using MediatR;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Commons
{
    public class IEntityBaseRequest : IRequest<bool>
    {
        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string LastModifiedUserId { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public bool IsDisplay { get; set; }
    }
}
