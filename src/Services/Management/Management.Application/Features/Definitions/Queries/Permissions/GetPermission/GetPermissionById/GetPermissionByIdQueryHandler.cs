﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.Permissions.GetPermission.GetPermissionById
{
    public class GetPermissionByIdQueryHandler : IRequestHandler<GetPermissionByIdQuery, PermissionViewModel>
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;

        public GetPermissionByIdQueryHandler(IPermissionRepository permissionRepository, IMapper mapper)
        {
            _permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<PermissionViewModel> Handle(GetPermissionByIdQuery request, CancellationToken cancellationToken)
        {
            var permissionEntity = await _permissionRepository.GetEntityByIdAsync(request.Id);
            return _mapper.Map<PermissionViewModel>(permissionEntity);
        }
    }
}
