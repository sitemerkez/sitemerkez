﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Features.Definitions.Queries.Permissions.GetPermissionsList.GetPermissionsListByAndIsDisplay;
using Management.Application.Models.Definitions;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.Permissions.GetPermissionsList.GetPermissionsListByIsDisplay
{
    public class GetPermissionsListByIsDisplayQueryHandler : IRequestHandler<GetPermissionsListByIsDisplayQuery, List<PermissionViewModel>>
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;

        public GetPermissionsListByIsDisplayQueryHandler(IPermissionRepository permissionRepository, IMapper mapper)
        {
            _permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<PermissionViewModel>> Handle(GetPermissionsListByIsDisplayQuery request, CancellationToken cancellationToken)
        {
            var permissionsList = await _permissionRepository.GetEntitiesAsync(p => p.IsDisplay == request.IsDisplay, p => p.OperationType);

            return _mapper.Map<List<PermissionViewModel>>(permissionsList);
        }
    }
}
