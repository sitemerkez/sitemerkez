﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.Permissions.GetPermissionsList.GetPermissionsListByAndIsDisplay
{
    public class GetPermissionsListByIsDisplayQuery : IRequest<List<PermissionViewModel>>
    {
        public bool IsDisplay { get; set; }

        public GetPermissionsListByIsDisplayQuery(bool isDisplay)
        {
            IsDisplay = isDisplay;
        }
    }
}
