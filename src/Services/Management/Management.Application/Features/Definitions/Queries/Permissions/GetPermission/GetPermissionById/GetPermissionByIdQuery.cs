﻿using Management.Application.Models.Definitions;
using MediatR;
using System;

namespace Management.Application.Features.Definitions.Queries.Permissions.GetPermission.GetPermissionById
{
    
  public class GetPermissionByIdQuery : IRequest<PermissionViewModel>
    {
        public int Id { get; set; }
  
        public GetPermissionByIdQuery(int id)
        {
            Id = id;
        }
    }
}
