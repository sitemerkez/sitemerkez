﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.Modules.GetList
{
    public class GetModulesListByFilterTextQuery : IRequest<List<ModuleViewModel>>
    {
        public string FilterText { get; set; }

        public GetModulesListByFilterTextQuery(string filterText)
        {
            FilterText = filterText;
        }
    }
}
