﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.Modules.Get.GetModuleById
{
    
  public class GetModuleByIdQuery : IRequest<ModuleViewModel>
    {
        public int Id { get; set; }
  
        public GetModuleByIdQuery(int id)
        {
            Id = id;
        }
    }
}
