﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.Modules.GetList
{
    public class GetModulesListByIsDisplayQuery : IRequest<List<ModuleViewModel>>
    {
        public bool IsDisplay { get; set; }

        public GetModulesListByIsDisplayQuery(bool isDisplay)
        {
            IsDisplay = isDisplay;
        }
    }
}
