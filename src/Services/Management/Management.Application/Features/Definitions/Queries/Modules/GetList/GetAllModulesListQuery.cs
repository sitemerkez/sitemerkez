﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.Modules.GetList
{
    public class GetAllModulesListQuery : IRequest<List<ModuleViewModel>>
    {
        public GetAllModulesListQuery()
        {
            
        }
    }
}
