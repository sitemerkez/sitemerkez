﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Models.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.Modules.GetList
{
    public class GetModulesListByIsDisplayQueryHandler : IRequestHandler<GetModulesListByIsDisplayQuery, List<ModuleViewModel>>
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IMapper _mapper;

        public GetModulesListByIsDisplayQueryHandler(IModuleRepository moduleRepository, IMapper mapper)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ModuleViewModel>> Handle(GetModulesListByIsDisplayQuery request, CancellationToken cancellationToken)
        {
            var modulesList = await _moduleRepository.GetEntitiesAsync(p=> p.IsDisplay == request.IsDisplay);

            var moduleTypeList = new List<ModuleType>(); // ModuleType.List();

            var list = modulesList.ToList();

            list.ForEach(c => c.ModuleType = (moduleTypeList.FirstOrDefault(p => p.Id == c.ModuleTypeId)));

            return _mapper.Map<List<ModuleViewModel>>(list);
        }
    }
}
