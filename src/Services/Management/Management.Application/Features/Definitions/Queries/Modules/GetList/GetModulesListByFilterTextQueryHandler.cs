﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.Modules.GetList
{
    public class GetModulesListByFilterTextQueryHandler : IRequestHandler<GetModulesListByFilterTextQuery, List<ModuleViewModel>>
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IMapper _mapper;

        public GetModulesListByFilterTextQueryHandler(IModuleRepository moduleRepository, IMapper mapper)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ModuleViewModel>> Handle(GetModulesListByFilterTextQuery request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(request.FilterText))
            {
                return _mapper.Map<List<ModuleViewModel>>(await _moduleRepository.GetAllEntitiesAsync());
            }
            var modulesList = await _moduleRepository.GetEntitiesAsync(p =>
            p.Name.ToLower().Contains(request.FilterText.ToLower()) ||
            p.Description.ToLower().Contains(request.FilterText.ToLower()), p => p.ModuleType);

            return _mapper.Map<List<ModuleViewModel>>(modulesList);
        }
    }
}
