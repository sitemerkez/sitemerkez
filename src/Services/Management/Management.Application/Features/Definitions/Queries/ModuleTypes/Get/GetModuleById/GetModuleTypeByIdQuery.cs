﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.Get.GetModuleTypeById
{
    
  public class GetModuleTypeByIdQuery : IRequest<ModuleTypeViewModel>
    {
        public int Id { get; set; }
  
        public GetModuleTypeByIdQuery(int id)
        {
            Id = id;
        }
    }
}
