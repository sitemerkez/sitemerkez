﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.Get.GetModuleTypeById
{
    public class GetModuleTypeByIdQueryHandler : IRequestHandler<GetModuleTypeByIdQuery, ModuleTypeViewModel>
    {
        private readonly IModuleTypeRepository _moduleRepository;
        private readonly IMapper _mapper;

        public GetModuleTypeByIdQueryHandler(IModuleTypeRepository moduleRepository, IMapper mapper)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ModuleTypeViewModel> Handle(GetModuleTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var moduleEntity = await _moduleRepository.GetEntityByIdAsync(request.Id);
            return _mapper.Map<ModuleTypeViewModel>(moduleEntity);
        }
    }
}
