﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.GetList
{
    public class GetAllModuleTypesListQuery : IRequest<List<ModuleTypeViewModel>>
    {
        public GetAllModuleTypesListQuery()
        {
            
        }
    }
}
