﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.GetList
{
    public class GetModuleTypesListByIsDisplayQuery : IRequest<List<ModuleTypeViewModel>>
    {
        public bool IsDisplay { get; set; }

        public GetModuleTypesListByIsDisplayQuery(bool isDisplay)
        {
            IsDisplay = isDisplay;
        }
    }
}
