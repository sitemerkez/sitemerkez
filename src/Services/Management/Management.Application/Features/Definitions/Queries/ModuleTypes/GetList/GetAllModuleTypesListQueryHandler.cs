﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Features.Definitions.Queries.ModuleTypes.GetList;
using Management.Application.Models.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.GetList
{
    public class GetModuleTypesListQueryHandler : IRequestHandler<GetAllModuleTypesListQuery, List<ModuleTypeViewModel>>
    {
        private readonly IModuleTypeRepository _moduleRepository;
        private readonly IMapper _mapper;

        public GetModuleTypesListQueryHandler(IModuleTypeRepository moduleRepository, IMapper mapper)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ModuleTypeViewModel>> Handle(GetAllModuleTypesListQuery request, CancellationToken cancellationToken)
        {
            var modulesList = await _moduleRepository.GetEntitiesAsync(null);

            return _mapper.Map<List<ModuleTypeViewModel>>(modulesList);
        }
    }
}
