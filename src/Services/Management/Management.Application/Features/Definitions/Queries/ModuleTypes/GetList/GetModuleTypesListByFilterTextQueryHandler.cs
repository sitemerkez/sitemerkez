﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.GetList
{
    public class GetModuleTypesListByFilterTextQueryHandler : IRequestHandler<GetModuleTypesListByFilterTextQuery, List<ModuleTypeViewModel>>
    {
        private readonly IModuleTypeRepository _moduleRepository;
        private readonly IMapper _mapper;

        public GetModuleTypesListByFilterTextQueryHandler(IModuleTypeRepository moduleRepository, IMapper mapper)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ModuleTypeViewModel>> Handle(GetModuleTypesListByFilterTextQuery request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(request.FilterText))
            {
                return _mapper.Map<List<ModuleTypeViewModel>>(await _moduleRepository.GetAllEntitiesAsync());
            }
            var modulesList = await _moduleRepository.GetEntitiesAsync(p =>
            p.Name.ToLower().Contains(request.FilterText.ToLower()));

            return _mapper.Map<List<ModuleTypeViewModel>>(modulesList);
        }
    }
}
