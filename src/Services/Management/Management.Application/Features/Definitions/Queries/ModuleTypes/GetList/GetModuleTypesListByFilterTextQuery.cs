﻿using Management.Application.Models.Definitions;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Definitions.Queries.ModuleTypes.GetList
{
    public class GetModuleTypesListByFilterTextQuery : IRequest<List<ModuleTypeViewModel>>
    {
        public string FilterText { get; set; }

        public GetModuleTypesListByFilterTextQuery(string filterText)
        {
            FilterText = filterText;
        }
    }
}
