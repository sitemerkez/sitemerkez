﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.UpdateModule
{
    public class UpdateModuleTypeCommand : IEntityBaseWithIdRequest
    {
        public string Name { get; set; }
        public int ModuleTypeEnumId { get; set; }
    }
}
