﻿using FluentValidation;
using Management.Application.Features.Commons;

namespace Management.Application.Features.Definitions.Commands.Modules.AddModule
{
    public class AddModuleTypeCommandValidator : EntityBaseAbstractValidator<AddModuleTypeCommand>
    {
        public AddModuleTypeCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Name} must not exceed 50 characters..");

            RuleFor(p => p.ModuleTypeEnumId)
               .NotEmpty().WithMessage("{ModuleTypeEnumId} is required.");

        }
    }
}
