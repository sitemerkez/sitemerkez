﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Management.Application.Features.Definitions.Commands.Modules.AddModule
{
    public class AddModuleTypeCommandHandler : IRequestHandler<AddModuleTypeCommand, bool>
    {
        private readonly IModuleTypeRepository _moduleTypeRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<AddModuleTypeCommandHandler> _logger;

        public AddModuleTypeCommandHandler(IModuleTypeRepository moduleTypeRepository, IMapper mapper, ILogger<AddModuleTypeCommandHandler> logger)
        {
            _moduleTypeRepository = moduleTypeRepository ?? throw new ArgumentNullException(nameof(moduleTypeRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddModuleTypeCommand request, CancellationToken cancellationToken)
        {
            var moduleEntity = _mapper.Map<ModuleType>(request);
            var newModuleType = await _moduleTypeRepository.AddEntityAsync(moduleEntity);

            _logger.LogInformation($"ModuleType {newModuleType.Id} is successfully created.");


            return true;
        }
    }
}
