﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.UpdateModule
{
    public class UpdateModuleTypeCommandHandler : IRequestHandler<UpdateModuleTypeCommand, bool>
    {
        private readonly IModuleTypeRepository _moduleTypeRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateModuleTypeCommandHandler> _logger;

        public UpdateModuleTypeCommandHandler(IModuleTypeRepository moduleTypeRepository, IMapper mapper, ILogger<UpdateModuleTypeCommandHandler> logger)
        {
            _moduleTypeRepository = moduleTypeRepository ?? throw new ArgumentNullException(nameof(moduleTypeRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateModuleTypeCommand request, CancellationToken cancellationToken)
        {
            var moduleTypeToUpdate = await _moduleTypeRepository.GetEntityByIdAsync(request.Id);
            if (moduleTypeToUpdate == null)
            {
                throw new NotFoundException(nameof(ModuleType), request.Id);
            }

            _mapper.Map(request, moduleTypeToUpdate, typeof(UpdateModuleTypeCommand), typeof(ModuleType));

            await _moduleTypeRepository.UpdateEntityAsync( moduleTypeToUpdate);

            _logger.LogInformation($"ModuleType {moduleTypeToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
