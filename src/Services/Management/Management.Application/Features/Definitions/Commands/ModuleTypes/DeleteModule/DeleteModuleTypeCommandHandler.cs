﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.DeleteModule
{
    public class DeleteModuleTypeCommandHandler : IRequestHandler<DeleteModuleTypeCommand, bool>
    {
        private readonly IModuleTypeRepository _moduleTypeRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteModuleTypeCommandHandler> _logger;

        public DeleteModuleTypeCommandHandler(IModuleTypeRepository moduleTypeRepository, IMapper mapper, ILogger<DeleteModuleTypeCommandHandler> logger)
        {
            _moduleTypeRepository = moduleTypeRepository ?? throw new ArgumentNullException(nameof(moduleTypeRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteModuleTypeCommand request, CancellationToken cancellationToken)
        {
            var moduleTypeToDelete = await _moduleTypeRepository.GetEntityByIdAsync(request.Id);
            if (moduleTypeToDelete == null)
            {
                throw new NotFoundException(nameof(ModuleType), request.Id);
            }

            await _moduleTypeRepository.DeleteEntityAsync(moduleTypeToDelete);
            _logger.LogInformation($"ModuleType {moduleTypeToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
