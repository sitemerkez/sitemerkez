﻿using FluentValidation;
using Management.Application.Features.Commons;

namespace Management.Application.Features.Definitions.Commands.Permissions.AddPermission
{
    public class AddPermissionCommandValidator : EntityBaseAbstractValidator<AddPermissionCommand>
    {
        public AddPermissionCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Name} must not exceed 50 characters..");

            RuleFor(p => p.OperationTypeId)
               .NotEmpty().WithMessage("{OperationTypeId} is required.");

        }
    }
}
