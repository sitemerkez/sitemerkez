﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.Application.Features.Commons;
using MediatR;

namespace Management.Application.Features.Definitions.Commands.Permissions.AddPermission
{
    public class AddPermissionCommand : IEntityBaseRequest
    {
        public string Name { get; set; }
        public int OperationTypeId { get; set; }
    }
}
