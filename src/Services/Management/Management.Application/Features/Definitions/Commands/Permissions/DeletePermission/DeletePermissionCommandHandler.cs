﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Permissions.DeletePermission
{
    public class DeletePermissionCommandHandler : IRequestHandler<DeletePermissionCommand, bool>
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<DeletePermissionCommandHandler> _logger;

        public DeletePermissionCommandHandler(IPermissionRepository permissionRepository, IMapper mapper, ILogger<DeletePermissionCommandHandler> logger)
        {
            _permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeletePermissionCommand request, CancellationToken cancellationToken)
        {
            var permissionToDelete = await _permissionRepository.GetEntityByIdAsync(request.Id);
            if (permissionToDelete == null)
            {
                throw new NotFoundException(nameof(Permission), request.Id);
            }

            await _permissionRepository.DeleteEntityAsync(permissionToDelete);
            _logger.LogInformation($"Permission {permissionToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
