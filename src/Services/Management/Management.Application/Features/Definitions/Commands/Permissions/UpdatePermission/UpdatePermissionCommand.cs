﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Permissions.UpdatePermission
{
    public class UpdatePermissionCommand : IEntityBaseWithIdRequest
    {
        public string Name { get; set; }
        public int OperationTypeId { get; set; }
    }
}
