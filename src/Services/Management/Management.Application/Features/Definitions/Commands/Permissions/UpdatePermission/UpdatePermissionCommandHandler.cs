﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Permissions.UpdatePermission
{
    public class UpdatePermissionCommandHandler : IRequestHandler<UpdatePermissionCommand, bool>
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdatePermissionCommandHandler> _logger;

        public UpdatePermissionCommandHandler(IPermissionRepository permissionRepository, IMapper mapper, ILogger<UpdatePermissionCommandHandler> logger)
        {
            _permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdatePermissionCommand request, CancellationToken cancellationToken)
        {
            var permissionToUpdate = await _permissionRepository.GetEntityByIdAsync(request.Id);
            if (permissionToUpdate == null)
            {
                throw new NotFoundException(nameof(Permission), request.Id);
            }

            _mapper.Map(request, permissionToUpdate, typeof(UpdatePermissionCommand), typeof(Permission));

            await _permissionRepository.UpdateEntityAsync(permissionToUpdate);

            _logger.LogInformation($"Permission {permissionToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
