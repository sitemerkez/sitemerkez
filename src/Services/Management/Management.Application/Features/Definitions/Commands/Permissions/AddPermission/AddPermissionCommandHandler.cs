﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Management.Application.Features.Definitions.Commands.Permissions.AddPermission
{
    public class AddPermissionCommandHandler : IRequestHandler<AddPermissionCommand, bool>
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<AddPermissionCommandHandler> _logger;

        public AddPermissionCommandHandler(IPermissionRepository permissionRepository, IMapper mapper, ILogger<AddPermissionCommandHandler> logger)
        {
            _permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddPermissionCommand request, CancellationToken cancellationToken)
        {
            var roleEntity = _mapper.Map<Permission>(request);
            var newPermission = await _permissionRepository.AddEntityAsync(roleEntity);

            _logger.LogInformation($"Permission {newPermission.Id} is successfully created.");


            return true;
        }
    }
}
