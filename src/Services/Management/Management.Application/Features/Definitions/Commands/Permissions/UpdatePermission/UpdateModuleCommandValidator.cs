﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Definitions.Commands.Permissions.AddPermission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Permissions.UpdatePermission
{
    public class UpdatePermissionCommandValidator : EntityBaseAbstractValidator<UpdatePermissionCommand>
    {
        public UpdatePermissionCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Name} must not exceed 50 characters..");

        }
    }
}
