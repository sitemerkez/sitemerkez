﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.DeleteModule
{
    public class DeleteModuleCommandHandler : IRequestHandler<DeleteModuleCommand, bool>
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteModuleCommandHandler> _logger;

        public DeleteModuleCommandHandler(IModuleRepository moduleRepository, IMapper mapper, ILogger<DeleteModuleCommandHandler> logger)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteModuleCommand request, CancellationToken cancellationToken)
        {
            var moduleToDelete = await _moduleRepository.GetEntityByIdAsync(request.Id);
            if (moduleToDelete == null)
            {
                throw new NotFoundException(nameof(Module), request.Id);
            }

            await _moduleRepository.DeleteEntityAsync(moduleToDelete);
            _logger.LogInformation($"Module {moduleToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
