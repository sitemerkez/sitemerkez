﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Application.Exceptions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.UpdateModule
{
    public class UpdateModuleCommandHandler : IRequestHandler<UpdateModuleCommand, bool>
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateModuleCommandHandler> _logger;

        public UpdateModuleCommandHandler(IModuleRepository moduleRepository, IMapper mapper, ILogger<UpdateModuleCommandHandler> logger)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateModuleCommand request, CancellationToken cancellationToken)
        {
            var moduleToUpdate = await _moduleRepository.GetEntityByIdAsync(request.Id);
            if (moduleToUpdate == null)
            {
                throw new NotFoundException(nameof(Module), request.Id);
            }

            _mapper.Map(request, moduleToUpdate, typeof(UpdateModuleCommand), typeof(Module));

            await _moduleRepository.UpdateEntityAsync( moduleToUpdate);

            _logger.LogInformation($"Module {moduleToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
