﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Management.Application.Features.Definitions.Commands.Modules.AddModule
{
    public class AddModuleCommandHandler : IRequestHandler<AddModuleCommand, bool>
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<AddModuleCommandHandler> _logger;

        public AddModuleCommandHandler(IModuleRepository moduleRepository, IMapper mapper, ILogger<AddModuleCommandHandler> logger)
        {
            _moduleRepository = moduleRepository ?? throw new ArgumentNullException(nameof(moduleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddModuleCommand request, CancellationToken cancellationToken)
        {
            var roleEntity = _mapper.Map<Module>(request);
            var newModule = await _moduleRepository.AddEntityAsync(roleEntity);

            _logger.LogInformation($"Module {newModule.Id} is successfully created.");


            return true;
        }
    }
}
