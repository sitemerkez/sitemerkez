﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Definitions.Commands.Modules.AddModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Definitions.Commands.Modules.UpdateModule
{
    public class UpdateModuleCommandValidator : EntityBaseAbstractValidator<UpdateModuleCommand>
    {
        public UpdateModuleCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Name} must not exceed 50 characters..");

        }
    }
}
