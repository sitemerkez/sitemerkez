﻿using FluentValidation;
using Management.Application.Features.Commons;

namespace Management.Application.Features.Authentications.Commands.Users.AddUser
{
    public class AddUserCommandValidator : EntityBaseAbstractValidator<AddUserCommand>
    {
        public AddUserCommandValidator()
        {
            RuleFor(p => p.Email)
                .NotEmpty().WithMessage("{Email} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{Email} must not exceed 50 characters..");

            RuleFor(p => p.Password)
               .NotEmpty().WithMessage("{Password} is required.");
          
        }
    }
}
