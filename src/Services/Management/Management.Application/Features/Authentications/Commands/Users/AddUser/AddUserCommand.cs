﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.Application.Features.Commons;
using MediatR;

namespace Management.Application.Features.Authentications.Commands.Users.AddUser
{
    public class AddUserCommand : IEntityBaseRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
