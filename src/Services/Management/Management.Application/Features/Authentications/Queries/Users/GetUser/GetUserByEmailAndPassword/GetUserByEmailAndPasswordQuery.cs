﻿using Management.Application.Models.Authentications;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Authentications.Queries.Users.GetUser
{
    public class GetUserByEmailAndPasswordQuery : IRequest<UserViewModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }


        public GetUserByEmailAndPasswordQuery(string email, string password)
        {
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Password = password ?? throw new ArgumentNullException(nameof(password));

        }
    }
}
