﻿using AutoMapper;
using Management.Application.Models.Authentications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Authentications.Queries.Users.GetUsersList.GetUserListByOrganizationId
{
    //public class GetUserListByOrganizationIdHandler : IRequestHandler<GetUserListByOrganizationIdQuery, List<UserViewModel>>
    //{
    //    private readonly IUserRepository _userRepository;
    //    private readonly IMapper _mapper;

    //    public GetUserListByOrganizationIdHandler(IUserRepository userRepository, IMapper mapper)
    //    {
    //        _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
    //        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    //    }

    //    public async Task<List<UserViewModel>> Handle(GetUserListByOrganizationIdQuery request, CancellationToken cancellationToken)
    //    {
    //        var usersList = await _userRepository.GetUsersListByOrganizationId(request.OrganizationId);
    //        return _mapper.Map<List<UserViewModel>>(usersList);
    //    }
    //}
}
