﻿using AutoMapper;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Application.Models.Authentications;
using Management.Domain.Entities.Concrete.Authentications;
using MediatR;
using Shared.Enumetarions.Definitions;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Authentications.Queries.Users.GetUser
{
    //public class GetUserByEmailAndPasswordQueryHandler : IRequestHandler<GetUserByEmailAndPasswordQuery, UserViewModel>
    //{
        //private readonly UserManager<User> _userManager;
        //private readonly SignInManager<User> _singInManager;
        //private readonly IMapper _mapper;
        //private readonly IOrganizationRepository _organizationRepository;
        //private readonly IEncryptionService _encryptionService;

        //public GetUserByEmailAndPasswordQueryHandler(UserManager<User> userManager,
        //    SignInManager<User> singInManager,
        //    IMapper mapper,
        //    IOrganizationRepository organizationRepository,
        //    IEncryptionService encryptionService)
        //{
        //    _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        //    _singInManager = singInManager ?? throw new ArgumentNullException(nameof(singInManager));
        //    _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        //    _organizationRepository = organizationRepository ?? throw new ArgumentNullException(nameof(organizationRepository));
        //    _encryptionService = encryptionService ?? throw new ArgumentNullException(nameof(encryptionService));
        //}

        //public async Task<UserViewModel> Handle(GetUserByEmailAndPasswordQuery request, CancellationToken cancellationToken)
        //{
        //    UserViewModel userViewModel = new UserViewModel();

        //    var userEntity = await _userManager.FindByEmailAsync(request.Email);

        //    if (userEntity != null )
        //    {
        //        var result = await _singInManager.PasswordSignInAsync(userEntity, request.Password, false, false);
        //        if(result.Succeeded)
        //        {
        //            userViewModel.OrganizationId = 1;
        //            userViewModel.OrganizationName = _organizationRepository.GetEntityByIdAsync(userViewModel.OrganizationId).Result.Name;
        //            userViewModel.RoleIds = new List<string> { "Admin" };
        //        }

        //    }

        //    return userViewModel;
        //}
    //}
}
