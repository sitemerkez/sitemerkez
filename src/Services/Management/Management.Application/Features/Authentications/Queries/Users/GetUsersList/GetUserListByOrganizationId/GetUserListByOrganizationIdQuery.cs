﻿using Management.Application.Models.Authentications;
using MediatR;
using System;
using System.Collections.Generic;

namespace Management.Application.Features.Authentications.Queries.Users.GetUsersList.GetUserListByOrganizationId
{
    public class GetUserListByOrganizationIdQuery : IRequest<List<UserViewModel>>
    {
        public int OrganizationId { get; set; }

        public GetUserListByOrganizationIdQuery(int organizationId)
        {
            OrganizationId = organizationId;
        }
    }
}
