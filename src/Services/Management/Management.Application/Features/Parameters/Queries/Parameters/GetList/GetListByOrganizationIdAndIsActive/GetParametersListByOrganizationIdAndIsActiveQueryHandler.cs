﻿using AutoMapper;
using Management.Application.Features.Parameters.Queries.Parameters.GetList.GetListByOrganizationIdAndIsDisplay;
using Management.Application.Models.Parameters;
using Management.Application.Services.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.Parameters.GetList.GetListByOrganizationId
{
    public class GetParametersListByOrganizationIdQueryHandler : IRequestHandler<GetParametersListByOrganizationIdQuery, List<ParameterViewModel>>
    {
        private readonly IParameterService _parameterService;
        private readonly IMapper _mapper;

        public GetParametersListByOrganizationIdQueryHandler(IParameterService parameterService, IMapper mapper)
        {
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ParameterViewModel>> Handle(GetParametersListByOrganizationIdQuery request, CancellationToken cancellationToken)
        {
            var parametersList = await _parameterService.GetParametersListByOrganizationId(request.OrganizationId);
            return _mapper.Map<List<ParameterViewModel>>(parametersList);
        }
    }
}
