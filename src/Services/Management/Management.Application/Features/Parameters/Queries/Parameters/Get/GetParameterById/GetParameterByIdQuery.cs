﻿using Management.Application.Models.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.Parameters.Get.GetParameterById
{
    public class GetParameterByIdQuery : IRequest<ParameterViewModel>
    {
        public int Id { get; set; }

        public GetParameterByIdQuery(int id)
        {
            Id = id;
        }
    }
}
