﻿using Management.Application.Models.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.Parameters.GetList.GetListByOrganizationIdAndIsDisplay
{
    public class GetParametersListByOrganizationIdQuery : IRequest<List<ParameterViewModel>>
    {
        public int OrganizationId { get; set; }

        public GetParametersListByOrganizationIdQuery(int organizationId)
        {
            OrganizationId = organizationId;
        }
    }
}
