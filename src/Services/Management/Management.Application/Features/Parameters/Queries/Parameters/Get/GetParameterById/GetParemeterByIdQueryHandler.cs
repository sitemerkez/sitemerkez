﻿using AutoMapper;
using Management.Application.Models.Parameters;
using Management.Application.Services.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.Parameters.Get.GetParameterById
{
    public class GetParameterByIdQueryHandler : IRequestHandler<GetParameterByIdQuery, ParameterViewModel>
    {
        private readonly IParameterService _parameterService;
        private readonly IMapper _mapper;

        public GetParameterByIdQueryHandler(IParameterService parameterService, IMapper mapper)
        {
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ParameterViewModel> Handle(GetParameterByIdQuery request, CancellationToken cancellationToken)
        {
            var parameterEntity = await _parameterService.GetEntityById(request.Id);
            return _mapper.Map<ParameterViewModel>(parameterEntity);
        }
    }
}
