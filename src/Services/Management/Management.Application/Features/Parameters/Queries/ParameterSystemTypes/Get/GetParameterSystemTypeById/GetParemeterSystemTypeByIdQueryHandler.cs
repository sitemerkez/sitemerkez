﻿using AutoMapper;
using Management.Application.Models.Parameters;
using Management.Application.Services.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.ParameterSystemTypes.Get.GetParameterSystemTypeById
{
    public class GetParameterSystemTypeByIdQueryHandler : IRequestHandler<GetParameterSystemTypeByIdQuery, ParameterSystemTypeViewModel>
    {
        private readonly IParameterSystemTypeService _parameterSystemTypeService;
        private readonly IMapper _mapper;

        public GetParameterSystemTypeByIdQueryHandler(IParameterSystemTypeService parameterSystemTypeService, IMapper mapper)
        {
            _parameterSystemTypeService = parameterSystemTypeService ?? throw new ArgumentNullException(nameof(parameterSystemTypeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ParameterSystemTypeViewModel> Handle(GetParameterSystemTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var parameterSystemTypeEntity = await _parameterSystemTypeService.GetEntityByIdAsync(request.Id);
            return _mapper.Map<ParameterSystemTypeViewModel>(parameterSystemTypeEntity);
        }
    }
}
