﻿using Management.Application.Models.Parameters;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Queries.ParameterSystemTypes.Get.GetParameterSystemTypeById
{
    public class GetParameterSystemTypeByIdQuery : IRequest<ParameterSystemTypeViewModel>
    {
        public int Id { get; set; }

        public GetParameterSystemTypeByIdQuery(int id)
        {
            Id = id;
        }
    }
}
