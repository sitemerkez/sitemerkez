﻿using AutoMapper;
using Management.Application.Exceptions;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.DeleteParameter
{
    public class DeleteParameterSystemTypeCommandHandler : IRequestHandler<DeleteParameterSystemTypeCommand, bool>
    {
        private readonly IParameterSystemTypeService _parameterSystemTypeService;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteParameterSystemTypeCommandHandler> _logger;

        public DeleteParameterSystemTypeCommandHandler(IParameterSystemTypeService parameterSystemTypeService, IMapper mapper, ILogger<DeleteParameterSystemTypeCommandHandler> logger)
        {
            _parameterSystemTypeService = parameterSystemTypeService ?? throw new ArgumentNullException(nameof(parameterSystemTypeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteParameterSystemTypeCommand request, CancellationToken cancellationToken)
        {
            var parameterSystemTypeToDelete = await _parameterSystemTypeService.GetEntityByIdAsync(request.Id);
            if (parameterSystemTypeToDelete == null)
            {
                throw new NotFoundException(nameof(ParameterSystemType), request.Id);
            }

            await _parameterSystemTypeService.DeleteEntityAsync(parameterSystemTypeToDelete.Id);
            _logger.LogInformation($"ParameterSystemType {parameterSystemTypeToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
