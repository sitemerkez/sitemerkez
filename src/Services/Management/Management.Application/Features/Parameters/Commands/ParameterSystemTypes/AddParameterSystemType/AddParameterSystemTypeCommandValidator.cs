﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Parameters.Commands.ParameterSystemTypes.AddParameter;

namespace Management.Application.Features.ParameterSystemTypes.Commands.ParameterSystemTypes.AddParameterSystemType
{
    public class AddParameterSystemTypeCommandValidator : EntityBaseAbstractValidator<AddParameterSystemTypeCommand>
    {
        public AddParameterSystemTypeCommandValidator()
        {
            RuleFor(p => p.ParameterId)
                .NotEmpty().WithMessage("{ParameterId} is required.");
               
            RuleFor(p => p.SystemTypeId)
               .NotEmpty().WithMessage("{SystemTypeId} is required.");

        }
    }
}
