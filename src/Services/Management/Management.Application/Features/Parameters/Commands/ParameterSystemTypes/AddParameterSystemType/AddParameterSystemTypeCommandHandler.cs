﻿using AutoMapper;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.AddParameter
{
    public class AddParameterSystemTypeCommandHandler : IRequestHandler<AddParameterSystemTypeCommand, bool>
    {
        private readonly IParameterSystemTypeService _parameterSystemTypeService;
        private readonly IMapper _mapper;
        private readonly ILogger<AddParameterSystemTypeCommandHandler> _logger;

        public AddParameterSystemTypeCommandHandler(IParameterSystemTypeService parameterSystemTypeService, IMapper mapper, ILogger<AddParameterSystemTypeCommandHandler> logger)
        {
            _parameterSystemTypeService = parameterSystemTypeService ?? throw new ArgumentNullException(nameof(parameterSystemTypeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddParameterSystemTypeCommand request, CancellationToken cancellationToken)
        {
            var roleEntity = _mapper.Map<ParameterSystemType>(request);
            var newParameterSystemType = await _parameterSystemTypeService.AddEntityAsync(roleEntity);

            _logger.LogInformation($"ParameterSystemType {newParameterSystemType.Id} is successfully created.");


            return true;
        }
    }
}
