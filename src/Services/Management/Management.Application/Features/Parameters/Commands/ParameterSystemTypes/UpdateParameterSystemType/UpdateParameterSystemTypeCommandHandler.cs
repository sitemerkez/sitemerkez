﻿using AutoMapper;
using Management.Application.Exceptions;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.UpdateParameter
{
    public class UpdateParameterSystemTypeCommandHandler : IRequestHandler<UpdateParameterSystemTypeCommand, bool>
    {
        private readonly IParameterSystemTypeService _parameterSystemTypeService;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateParameterSystemTypeCommandHandler> _logger;

        public UpdateParameterSystemTypeCommandHandler(IParameterSystemTypeService parameterSystemTypeService, IMapper mapper, ILogger<UpdateParameterSystemTypeCommandHandler> logger)
        {
            _parameterSystemTypeService = parameterSystemTypeService ?? throw new ArgumentNullException(nameof(parameterSystemTypeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateParameterSystemTypeCommand request, CancellationToken cancellationToken)
        {
            var parameterSystemTypeToUpdate = await _parameterSystemTypeService.GetEntityByIdAsync(request.Id);
            if (parameterSystemTypeToUpdate == null)
            {
                throw new NotFoundException(nameof(ParameterSystemType), request.Id);
            }

            _mapper.Map(request, parameterSystemTypeToUpdate, typeof(UpdateParameterSystemTypeCommand), typeof(ParameterSystemType));

            await _parameterSystemTypeService.UpdateEntityAsync( parameterSystemTypeToUpdate);

            _logger.LogInformation($"ParameterSystemType {parameterSystemTypeToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
