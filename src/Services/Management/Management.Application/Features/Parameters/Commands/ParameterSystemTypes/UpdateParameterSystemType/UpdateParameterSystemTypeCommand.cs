﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.UpdateParameter
{
    public class UpdateParameterSystemTypeCommand : IEntityBaseWithIdRequest
    {
        public string ParametreId { get; set; }
        public int SystemTypeId { get; set; }
    }
}
