﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.AddParameter
{
    public class AddParameterSystemTypeCommand : IEntityBaseRequest
    {
        public string ParameterId { get; set; }
        public int SystemTypeId { get; set; }
    }
}
