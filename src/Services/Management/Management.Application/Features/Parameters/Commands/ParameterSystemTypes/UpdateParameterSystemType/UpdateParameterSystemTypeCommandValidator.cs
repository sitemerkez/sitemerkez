﻿using FluentValidation;
using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.ParameterSystemTypes.UpdateParameter
{
    public class UpdateParameterSystemTypeCommandValidator : EntityBaseAbstractValidator<UpdateParameterSystemTypeCommand>
    {
        public UpdateParameterSystemTypeCommandValidator()
        {
            RuleFor(p => p.ParametreId).NotEmpty().WithMessage("{ParametreId} is required.").NotNull();
            RuleFor(p => p.SystemTypeId).NotEmpty().WithMessage("{SystemTypeId} is required.").NotNull();
        }
    }
}
