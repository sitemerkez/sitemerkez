﻿using AutoMapper;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.Parameters.AddParameter
{
    public class AddParameterCommandHandler : IRequestHandler<AddParameterCommand, bool>
    {
        private readonly IParameterService _parameterService;
        private readonly IMapper _mapper;
        private readonly ILogger<AddParameterCommandHandler> _logger;

        public AddParameterCommandHandler(IParameterService parameterService, IMapper mapper, ILogger<AddParameterCommandHandler> logger)
        {
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(AddParameterCommand request, CancellationToken cancellationToken)
        {
            var roleEntity = _mapper.Map<Parameter>(request);
            var newParameter = await _parameterService.AddEntityAsync(roleEntity);

            _logger.LogInformation($"Parameter {newParameter.Id} is successfully created.");


            return true;
        }
    }
}
