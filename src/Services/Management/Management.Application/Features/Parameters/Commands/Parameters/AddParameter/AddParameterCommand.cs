﻿using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.Parameters.AddParameter
{
    public class AddParameterCommand : IOrganizationEntityBaseRequest
    {
        public string Name { get; set; }
        public int DataTypeId { get; set; }
        public int Maximum { get; set; }
        public int DisplayOrder { get; set; }
    }
}
