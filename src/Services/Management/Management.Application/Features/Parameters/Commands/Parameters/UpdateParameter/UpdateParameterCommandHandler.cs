﻿using AutoMapper;
using Management.Application.Exceptions;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.Parameters.UpdateParameter
{
    public class UpdateParameterCommandHandler : IRequestHandler<UpdateParameterCommand, bool>
    {
        private readonly IParameterService _parameterService;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateParameterCommandHandler> _logger;

        public UpdateParameterCommandHandler(IParameterService parameterService, IMapper mapper, ILogger<UpdateParameterCommandHandler> logger)
        {
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(UpdateParameterCommand request, CancellationToken cancellationToken)
        {
            var parameterToUpdate = await _parameterService.GetEntityById(request.Id);
            if (parameterToUpdate == null)
            {
                throw new NotFoundException(nameof(Parameter), request.Id);
            }

            _mapper.Map(request, parameterToUpdate, typeof(UpdateParameterCommand), typeof(Parameter));

            _parameterService.UpdateEntityAsync( parameterToUpdate);

            _logger.LogInformation($"Parameter {parameterToUpdate.Id} is successfully updated.");

            return true;
        }
    }
}
