﻿using FluentValidation;
using Management.Application.Features.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.Parameters.UpdateParameter
{
    public class UpdateParameterCommandValidator : EntityBaseAbstractValidator<UpdateParameterCommand>
    {
        public UpdateParameterCommandValidator()
        {
            RuleFor(p => p.Name).NotEmpty().WithMessage("{Name} is required.").NotNull();
            RuleFor(p => p.DataTypeId).NotEmpty().WithMessage("{DataTypeId} is required.").NotNull();
        }
    }
}
