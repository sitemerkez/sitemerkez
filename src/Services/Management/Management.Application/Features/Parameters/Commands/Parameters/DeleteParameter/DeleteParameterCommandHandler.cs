﻿using AutoMapper;
using Management.Application.Exceptions;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Application.Features.Parameters.Commands.Parameters.DeleteParameter
{
    public class DeleteParameterCommandHandler : IRequestHandler<DeleteParameterCommand, bool>
    {
        private readonly IParameterService _parameterService;
        private readonly IMapper _mapper;
        private readonly ILogger<DeleteParameterCommandHandler> _logger;

        public DeleteParameterCommandHandler(IParameterService parameterService, IMapper mapper, ILogger<DeleteParameterCommandHandler> logger)
        {
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> Handle(DeleteParameterCommand request, CancellationToken cancellationToken)
        {
            var parameterToDelete = await _parameterService.GetEntityById(request.Id);
            if (parameterToDelete == null)
            {
                throw new NotFoundException(nameof(Parameter), request.Id);
            }

            _parameterService.DeleteEntityAsync(parameterToDelete);
            _logger.LogInformation($"Parameter {parameterToDelete.Id} is successfully deleted.");

            return true;
        }
    }
}
