﻿using FluentValidation;
using Management.Application.Features.Commons;
using Management.Application.Features.Parameters.Commands.Parameters.AddParameter;

namespace Management.Application.Features.Parameters.Commands.Parameters.AddParameter
{
    public class AddParameterCommandValidator : EntityBaseAbstractValidator<AddParameterCommand>
    {
        public AddParameterCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{Name} is required.");
               
            RuleFor(p => p.DataTypeId)
               .NotEmpty().WithMessage("{DataTypeId} is required.");

        }
    }
}
