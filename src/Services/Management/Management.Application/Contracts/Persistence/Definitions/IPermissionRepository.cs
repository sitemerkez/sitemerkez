﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Management.Application.Contracts.Persistence.Definitions
{
    public interface IPermissionRepository : IBaseRepository<Permission>
    {
        
    }
}
