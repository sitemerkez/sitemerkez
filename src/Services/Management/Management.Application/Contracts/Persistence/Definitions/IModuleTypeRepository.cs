﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Definitions;

namespace Management.Application.Contracts.Persistence.Definitions
{
    public interface IModuleTypeRepository : IBaseRepository<ModuleType>
    {
        
    }
}
