﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Authentications;

namespace Management.Application.Contracts.Persistence.Authentications
{
    public interface IRolePermissionRepository : IBaseRepository<RolePermission>
    {
    }
}
