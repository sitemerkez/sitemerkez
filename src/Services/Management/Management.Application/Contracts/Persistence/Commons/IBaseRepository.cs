﻿
using Management.Domain.Entities.Concrete.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Management.Application.Contracts.Persistence.Commons
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllEntitiesAsync();
        Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate = null,
                                        Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                        string includeString = null,
                                        bool disableTracking = true);
        Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate = null,
                                       params Expression<Func<T, object>>[] including);
        Task<T> GetEntityByIdAsync(int id);
        Task<IEnumerable<T>> GetEntitiesByIdsAsync(int[] ids);
        Task<T> AddEntityAsync(T entity);
        Task AddEntitiesAsync(IEnumerable<T> entities);
        Task UpdateEntityAsync(T entity);
        Task UpdateEntitiesAsync(IEnumerable<T> entities);
        Task DeleteEntityAsync(T entity);
        Task DeleteEntitiesAsync(IEnumerable<T> entities);

        Task<int> CountEntitiesAsync();

        /// <summary>
        /// Gets a table
        /// </summary>
        IQueryable<T> Table { get; }

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        IQueryable<T> TableNoTracking { get; }
    }
}
