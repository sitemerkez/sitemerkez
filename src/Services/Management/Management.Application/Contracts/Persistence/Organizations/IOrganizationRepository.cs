﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Organizations;

namespace Management.Application.Contracts.Persistence.Organizations
{
    public interface IOrganizationRepository : IBaseRepository<Organization>
    {
    }
}
