﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Application.Contracts.Persistence.Parameters
{
    public interface IParameterSystemTypeRepository : IBaseRepository<ParameterSystemType>
    {
        
    }
}
