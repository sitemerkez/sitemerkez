﻿using Management.Application.Contracts.Persistence.Definitions;
using Management.Domain.Entities.Concrete.Definitions;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;

namespace Management.Infrastructure.Repositories.Definitions
{
    public class ModuleRepository : BaseRepository<Module>, IModuleRepository
    {
        public ModuleRepository(ApplicationContext dbContext) : base(dbContext)
        {
            
        }
    }
}
