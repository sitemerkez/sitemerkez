﻿using Management.Application.Contracts.Persistence.Definitions;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;

namespace Management.Infrastructure.Repositories.Definitions
{
    public class PermissionRepository : BaseRepository<Permission>, IPermissionRepository
    {
        public PermissionRepository(ApplicationContext dbContext) : base(dbContext)
        {
            
        }
    }
}
