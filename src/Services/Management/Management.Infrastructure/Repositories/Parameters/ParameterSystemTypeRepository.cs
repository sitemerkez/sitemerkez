﻿using Management.Application.Contracts.Persistence.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;
using Management.Infrastructure.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Repositories.Parameters
{
    public class ParameterSystemTypeRepository : BaseRepository<ParameterSystemType>, IParameterSystemTypeRepository
    {
        public ParameterSystemTypeRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}
