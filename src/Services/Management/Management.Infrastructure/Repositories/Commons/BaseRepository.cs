﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Domain.Entities.Concrete.Commons;
using Management.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Management.Infrastructure.Repositories.Commons
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationContext _dbContext;

        public BaseRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        #region Properties

        public DbSet<T> Entities
        {
            get
            {
                return _dbContext.Set<T>();
            }
        }
        /// <summary>
        /// Gets a table
        /// </summary>
        public virtual IQueryable<T> Table
        {
            get
            {
                return _dbContext.Set<T>();
            }
        }

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        public virtual IQueryable<T> TableNoTracking
        {
            get
            {
                return _dbContext.Set<T>().AsNoTracking();
            }
        }


        #endregion

        #region Insert

        public virtual async Task<T> AddEntityAsync(T entity)
        {
            Entities.Add(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public virtual async Task AddEntitiesAsync(IEnumerable<T> entities)
        {
            await Entities.AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
        }

        #endregion

        #region Select 

        public async Task<IEnumerable<T>> GetAllEntitiesAsync()
        {
            return await Entities.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate)
        {
            return await Entities.Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeString = null, bool disableTracking = true)
        {
            IQueryable<T> query = Entities;
            if (disableTracking) query = query.AsNoTracking();

            if (!string.IsNullOrWhiteSpace(includeString)) query = query.Include(includeString);

            if (predicate != null) query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).ToListAsync();
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetEntitiesAsync(Expression<Func<T, bool>> predicate = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = Entities;
            query = query.AsNoTracking();

            if (includes != null) query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (predicate != null) query = query.Where(predicate);

           
            return await query.ToListAsync();
        }

        public virtual async Task<T> GetEntityByIdAsync(int id)
        {
            return await Entities.FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> GetEntitiesByIdsAsync(int[] ids)
        {
            return await Entities.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<int> CountEntitiesAsync()
        {
            return await Entities.CountAsync();
        }

        #endregion

        #region Update

        public async Task UpdateEntityAsync(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateEntitiesAsync(IEnumerable<T> entities)
        {
            _dbContext.Entry(entities).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        #endregion

        #region Delete 

        public async Task DeleteEntityAsync(T entity)
        {
            Entities.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteEntitiesAsync(IEnumerable<T> entities)
        {
            Entities.RemoveRange(entities);
            await _dbContext.SaveChangesAsync();
        }

        #endregion
    }
}
