﻿using Management.Application.Contracts.Persistence.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;
using Management.Infrastructure.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Repositories.Organizations
{
    public class OrganizationRepository : BaseRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}
