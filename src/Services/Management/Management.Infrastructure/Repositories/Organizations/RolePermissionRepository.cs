﻿using Management.Application.Contracts.Persistence.Authentications;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;

namespace Management.Infrastructure.Repositories.Organizations
{
    public class RolePermissionRepository : BaseRepository<RolePermission>, IRolePermissionRepository
    {
        public RolePermissionRepository(ApplicationContext dbContext) : base(dbContext)
        {
            
        }
    }
}
