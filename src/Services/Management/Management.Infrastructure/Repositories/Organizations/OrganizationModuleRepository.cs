﻿using Management.Application.Contracts.Persistence.Organizations;
using Management.Domain.Entities.Concrete.Organizations;
using Management.Infrastructure.Persistence;
using Management.Infrastructure.Repositories.Commons;
using Management.Infrastructure.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Repositories.Organizations
{
    //public class OrganizationModuleRepository : BaseRepository<OrganizationModule>, IOrganizationModuleRepository
    //{
    //    public OrganizationModuleRepository(ApplicationContext dbContext) : base(dbContext)
    //    {
    //    }

    //    public async Task<IEnumerable<OrganizationModule>> GetOrganizationModulesListByOrganizationId(int organizationId)
    //    {
    //        Expression<Func<OrganizationModule, bool>> predicate = p => p.OrganizationId == organizationId;

    //        return await GetEntitiesAsync(predicate);
    //    }
    //}
}
