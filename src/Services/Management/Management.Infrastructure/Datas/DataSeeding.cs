﻿using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using Management.Infrastructure.Datas.Providers.Definitions;
using Management.Infrastructure.Modules.Providers.Definitions;
using Management.Infrastructure.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Enumetarions.Definitions;
using Shared.Services;
using System.Collections.Generic;

namespace Management.Infrastructure.Datas
{
    public class DataSeeding
    {

        public static void Seed(IApplicationBuilder app)
        {
            var _scope = app.ApplicationServices.CreateScope();
            var _context = _scope.ServiceProvider.GetService<ApplicationContext>();
            var _encryptionService = _scope.ServiceProvider.GetService<IEncryptionService>();

            _context.Database.Migrate();

            #region Users

            if (_context.Users.CountAsync().Result == 0)
            {
                User userEntity = new User();

                userEntity.Name = "Erdi";
                userEntity.Surname = "Demir";
                userEntity.UserName = "edemir";
                userEntity.Email = "erdi.demir@icloud.com";
                userEntity.MobilePhone = "+905397341772";
                userEntity.Password = "Password77!!";
                userEntity.PasswordSalt = _encryptionService.CreateSaltKey(8);
                userEntity.Password = _encryptionService.CreatePasswordHash(userEntity.Password, userEntity.PasswordSalt, "SHA256");
                userEntity.CreatedUserId = 1;
                userEntity.LastModifiedUserId = 1;
                userEntity.CreatedDate = System.DateTime.Now;
                userEntity.LastModifiedDate = System.DateTime.Now;
                userEntity.IsDisplay = true;
                userEntity.DisplayOrder = 1;


                _context.Users.Add(userEntity);
                _context.SaveChanges();
            }


            #endregion



            #region Definitions

            #region DataTypes

            if (_context.DataTypes.CountAsync().Result == 0)
            {
                var adminUser = _context.Users.FirstAsync(p => p.Email == "erdi.demir@icloud.com").Result;


                var dataTypesList = new DataTypeProvider(adminUser.Id).GetDataTypes();

                _context.DataTypes.AddRange(dataTypesList);
                _context.SaveChanges();
            }

            #endregion

            #region ModuleTypes

            if (_context.ModuleTypes.CountAsync().Result == 0)
            {
                var adminUser = _context.Users.FirstAsync(p => p.Email == "erdi.demir@icloud.com").Result;


                var dataTypesList = new ModuleTypeProvider(adminUser.Id).GetModuleTypes();

                _context.ModuleTypes.AddRange(dataTypesList);
                _context.SaveChanges();
            }

            #endregion

            #region Modules

            if (_context.Modules.CountAsync().Result == 0)
            {
                var adminUser = _context.Users.FirstAsync(p => p.Email == "erdi.demir@icloud.com").Result;
                var moduleTypesList = _context.ModuleTypes.ToListAsync().Result;

                List<Module> modulesList = new List<Module>();

                foreach (var item in moduleTypesList)
                {
                    switch (item.ModuleTypeEnumId)
                    {
                        case (int)ModuleTypeEnum.User:
                            {
                                var module = new Module()
                                {
                                    ModuleTypeId = (int)ModuleTypeEnum.User,
                                    Name = "Kullanıcı Modülü",
                                    Description = "Kullanıcı Modülü",
                                    CreatedUserId = adminUser.Id,
                                    CreatedDate = System.DateTime.Now,
                                    LastModifiedUserId = 1,
                                    LastModifiedDate = System.DateTime.Now,
                                    IsDisplay = true,
                                    DisplayOrder = adminUser.Id
                                };
                                modulesList.Add(module);
                            }
                            break;

                        case (int)ModuleTypeEnum.HR:
                            {
                                var module = new Module()
                                {
                                    ModuleTypeId = (int)ModuleTypeEnum.HR,
                                    Name = "İnsan Kaynakları Modülü",
                                    Description = "İnsan Kaynakları Modülü",
                                    CreatedUserId = adminUser.Id,
                                    CreatedDate = System.DateTime.Now,
                                    LastModifiedUserId = 1,
                                    LastModifiedDate = System.DateTime.Now,
                                    IsDisplay = true,
                                    DisplayOrder = adminUser.Id
                                };
                                modulesList.Add(module);
                            }
                            break;

                        case (int)ModuleTypeEnum.CRM:
                            {
                                var module = new Module()
                                {
                                    ModuleTypeId = (int)ModuleTypeEnum.CRM,
                                    Name = "CRM Modülü",
                                    Description = "CRM Modülü",
                                    CreatedUserId = adminUser.Id,
                                    CreatedDate = System.DateTime.Now,
                                    LastModifiedUserId = 1,
                                    LastModifiedDate = System.DateTime.Now,
                                    IsDisplay = true,
                                    DisplayOrder = adminUser.Id
                                };
                                modulesList.Add(module);
                            }
                            break;

                        case (int)ModuleTypeEnum.Accounting:
                            {
                                var module = new Module()
                                {
                                    ModuleTypeId = (int)ModuleTypeEnum.Accounting,
                                    Name = "Muhasebe Modülü",
                                    Description = "Muhasebe Modülü",
                                    CreatedUserId = adminUser.Id,
                                    CreatedDate = System.DateTime.Now,
                                    LastModifiedUserId = 1,
                                    LastModifiedDate = System.DateTime.Now,
                                    IsDisplay = true,
                                    DisplayOrder = adminUser.Id
                                };
                                modulesList.Add(module);
                            }
                            break;

                        case (int)ModuleTypeEnum.EShopping:
                            {
                                var module = new Module()
                                {
                                    ModuleTypeId = (int)ModuleTypeEnum.EShopping,
                                    Name = "E-Ticaret Modülü",
                                    Description = "E-Ticaret Modülü",
                                    CreatedUserId = adminUser.Id,
                                    CreatedDate = System.DateTime.Now,
                                    LastModifiedUserId = 1,
                                    LastModifiedDate = System.DateTime.Now,
                                    IsDisplay = true,
                                    DisplayOrder = adminUser.Id
                                };
                                modulesList.Add(module);
                            }
                            break;
                    }
                }

                _context.Modules.AddRange(modulesList);

                _context.SaveChanges();

            }

            #endregion



            #region Languages

            if (_context.Languages.CountAsync().Result == 0)
            {
                var adminUser = _context.Users.FirstAsync(p => p.Email == "erdi.demir@icloud.com");

                _context.Languages.AddRange(
                        new List<Language>() {
                         new Language() {
                             Name="Türkçe",
                             Code = "tr-TR",
                             CreatedUserId = 1,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = 1,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 1
                         },
                         new Language() {
                             Name="İngilizce",
                             Code = "en-US",
                             CreatedUserId = 1,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = 1,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 2}
                        }
                        );

                _context.SaveChanges();
            }

            #endregion

            #region Translates

            if (_context.Translates.CountAsync().Result == 0)
            {
                var adminUser = _context.Users.FirstAsync(p => p.Email == "erdi.demir@icloud.com").Result;
                var turkce = _context.Languages.FirstAsync(p => p.Name == "Türkçe").Result;
                var ingilizce = _context.Languages.FirstAsync(p => p.Name == "İngilizce").Result;

                _context.Translates.AddRange(
                        new List<Translate>() {
                         new Translate() {
                             LanguageId = turkce.Id,
                             Code="email",
                             Value="E posta",
                             CreatedUserId = adminUser.Id,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = 1,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = adminUser.Id},

                         new Translate() {
                             LanguageId = turkce.Id,
                             Code="password",
                             Value="Şifre",
                             CreatedUserId = adminUser.Id,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = adminUser.Id,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 2},
                         new Translate() {
                             LanguageId = turkce.Id,
                             Code="log",
                             Value="Şifre",
                             CreatedUserId = adminUser.Id,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = adminUser.Id,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 3},

                         new Translate() {
                             LanguageId = ingilizce.Id,
                             Code="email",
                             Value="Email",
                             CreatedUserId = adminUser.Id,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = adminUser.Id,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 4},

                         new Translate() {
                             LanguageId = ingilizce.Id,
                             Code="password",
                             Value="Password",
                             CreatedUserId = adminUser.Id,
                             CreatedDate = System.DateTime.Now,
                             LastModifiedUserId = adminUser.Id,
                             LastModifiedDate=System.DateTime.Now,
                             IsDisplay = true,
                             DisplayOrder = 5},
                        }
                        );

                _context.SaveChanges();

            }

            #endregion



            #endregion


        }

        //[Obsolete]
        //public static void Seed(IConfiguration configuration)
        //{



        //    //    MongoDbSettings settings = new MongoDbSettings();
        //    //    settings.ConnectionString = configuration
        //    //            .GetSection(nameof(MongoDbSettings) + ":" + MongoDbSettings.ConnectionStringValue).Value;
        //    //    settings.Database = configuration
        //    //            .GetSection(nameof(MongoDbSettings) + ":" + MongoDbSettings.DatabaseValue).Value;

        //    //    var client = new MongoClient(settings.ConnectionString);
        //    //    var db = client.GetDatabase(settings.Database);

        //    //    IMongoCollection<Permission> _collection = db.GetCollection<Permission>(typeof(Permission).Name.ToLowerInvariant());

        //    //    long permissionCount = _collection.Count(p => p.IsDisplay == true || p.IsDisplay == false);

        //    //    var permissionList = PermissionProvider.GetPermissions();

        //    //    if ((int)permissionCount != permissionList.Count())
        //    //    {
        //    //        foreach (var item in permissionList)
        //    //        {
        //    //            permissionCount = _collection.Count(p => p.Name == item.Name);

        //    //            if (permissionCount == 0)
        //    //            {
        //    //                Permission permission = new Permission();

        //    //                permission.Name = item.Name;
        //    //                permission.OperationTypeId = item.OperationTypeId;
        //    //                permission.ParentPermissionName = item.ParentPermissionName;
        //    //                permission.CreatedUserId = "61025c502b7e2bdf65b3e31c";
        //    //                permission.CreatedDate = DateTime.Now;
        //    //                permission.LastModifiedUserId = "61025c502b7e2bdf65b3e31c";
        //    //                permission.LastModifiedDate = DateTime.Now;
        //    //                permission.IsDisplay = true;

        //    //                var options = new InsertOneOptions { BypassDocumentValidation = false };
        //    //                _collection.InsertOne(permission, options);
        //    //            }
        //    //        }
        //    //    }

        //}
    }
}
