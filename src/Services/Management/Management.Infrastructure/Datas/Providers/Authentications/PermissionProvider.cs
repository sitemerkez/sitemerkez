﻿using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Datas.Proivders.Authentications
{
    public static class PermissionProvider
    {
        private static User _adminUser;
        private static List<OperationType> _operationTypesList;

        public static void Init(User adminUser, List<OperationType> operationTypesList)
        {
            _adminUser = adminUser;
            _operationTypesList = operationTypesList;
        }

        #region Organizations

        #region Organizations

        public static readonly Permission OrganizationManage =
            new Permission
            {
                Name = "Organization.Manage",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Manage).FirstOrDefault().Id,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 1
            };

        public static readonly Permission OrganizationAdd =
            new Permission
            {
                Name = "Organization.Insert",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Insert).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 2
            };

        public static readonly Permission OrganizationGet =
            new Permission
            {
                Name = "Organization.Select",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Select).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 3
            };

        public static readonly Permission OrganizationUpdate =
            new Permission
            {
                Name = "Organization.Update",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Update).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 4
            };

        public static readonly Permission OrganizationDelete =
            new Permission
            {
                Name = "Organization.Delete",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Delete).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 5
            };


        #endregion

        #region Roles

        public static readonly Permission RoleManage =
            new Permission
            {
                Name = "Role.Manage",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Manage).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 1
            };

        public static readonly Permission RoleAdd =
            new Permission
            {
                Name = "Role.Insert",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Insert).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 2
            };

        public static readonly Permission RoleGet =
            new Permission
            {
                Name = "Role.Get",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Select).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 3
            };

        public static readonly Permission RoleUpdate =
            new Permission
            {
                Name = "Role.Update",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Update).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 4
            };

        public static readonly Permission RoleDelete =
            new Permission
            {
                Name = "Role.Delete",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Delete).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 5
            };


        #endregion

        #endregion

        #region Definitions

        #region Module

        public static readonly Permission ModuleManage =
            new Permission
            {
                Name = "Module.Manage",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Manage).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 1
            };

        public static readonly Permission ModuleAdd =
            new Permission
            {
                Name = "Module.Insert",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Insert).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 2
            };

        public static readonly Permission ModuleGet =
            new Permission
            {
                Name = "Module.Get",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Select).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 3
            };

        public static readonly Permission ModuleUpdate =
            new Permission
            {
                Name = "Module.Update",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Update).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 4
            };

        public static readonly Permission ModuleDelete =
            new Permission
            {
                Name = "Module.Delete",
                OperationTypeId = _operationTypesList.Where(p => p.OperationTypeEnumId == (int)OperationTypeEnum.Delete).FirstOrDefault().Id,
                CreatedUserId = _adminUser.Id,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _adminUser.Id,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 5
            };


        #endregion



        #endregion


        public static IEnumerable<Permission> GetPermissions()
        {

            return new[]
            {
                #region Organizations

                #region Organizations
                    OrganizationManage,
                    OrganizationAdd,
                    OrganizationGet,
                    OrganizationUpdate,
                    OrganizationDelete,
                #endregion

                #region Roles
                    RoleManage,
                    RoleAdd,
                    RoleGet,
                    RoleUpdate,
                    RoleDelete,
                #endregion

                #endregion

                #region Definitions

                #region Modules
                    ModuleManage,
                    ModuleAdd,
                    ModuleGet,
                    ModuleUpdate,
                    ModuleDelete,
                #endregion

                #endregion
            };
        }
    }
}
