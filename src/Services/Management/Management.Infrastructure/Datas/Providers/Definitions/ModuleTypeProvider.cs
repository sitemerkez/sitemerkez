﻿using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Modules.Providers.Definitions
{
    public class ModuleTypeProvider
    {
        public static int _userId = 0;

        public ModuleTypeProvider(int userId)
        {
            _userId = userId;
        }



        public List<ModuleType> GetModuleTypes()
        {
            ModuleType ModuleTypeUser = new ModuleType
            {
                Name = "Kullanıcı",
                ModuleTypeEnumId = (int)ModuleTypeEnum.User,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 1
            };

            ModuleType ModuleTypeHR = new ModuleType
            {
                Name = "İnsan Kaynakları",
                ModuleTypeEnumId = (int)ModuleTypeEnum.HR,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 2
            };

            ModuleType ModuleTypeCRM = new ModuleType
            {
                Name = "CRM",
                ModuleTypeEnumId = (int)ModuleTypeEnum.CRM,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 3
            };

            ModuleType ModuleTypeAccounting = new ModuleType
            {
                Name = "Muhasebe",
                ModuleTypeEnumId = (int)ModuleTypeEnum.Accounting,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 4
            };

            ModuleType ModuleTypeEShopping = new ModuleType
            {
                Name = "E-Ticaret",
                ModuleTypeEnumId = (int)ModuleTypeEnum.EShopping,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 5
            };


            return new[]
            {
                ModuleTypeUser,
                ModuleTypeHR,
                ModuleTypeCRM,
                ModuleTypeAccounting,
                ModuleTypeEShopping,
            }.OrderBy(p => p.ModuleTypeEnumId).ToList();
        }
    }
}
