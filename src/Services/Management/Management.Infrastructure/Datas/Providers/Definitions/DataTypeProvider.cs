﻿using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Definitions;
using Shared.Enumetarions.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Datas.Providers.Definitions
{
    public class DataTypeProvider
    {
        public static int _userId = 0;

        public DataTypeProvider(int userId)
        {
            _userId = userId;
        }



        public List<DataType> GetDataTypes()
        {
            DataType DataTypeText = new DataType
            {
                Name = "Text",
                DataTypeEnumId = (int)DataTypeEnum.Text,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 1
            };

            DataType DataTypeTextLimited = new DataType
            {
                Name = "TextLimited",
                DataTypeEnumId = (int)DataTypeEnum.TextLimited,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 2
            };

            DataType DataTypeInt = new DataType
            {
                Name = "Int",
                DataTypeEnumId = (int)DataTypeEnum.Int,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 3
            };

            DataType DataTypeDecimal = new DataType
            {
                Name = "Decimal",
                DataTypeEnumId = (int)DataTypeEnum.Decimal,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 4
            };

            DataType DataTypeDatetime = new DataType
            {
                Name = "Datetime",
                DataTypeEnumId = (int)DataTypeEnum.Datetime,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 5
            };

            DataType DataTypeBit = new DataType
            {
                Name = "Bit",
                DataTypeEnumId = (int)DataTypeEnum.Bit,
                CreatedUserId = _userId,
                CreatedDate = DateTime.Now,
                LastModifiedUserId = _userId,
                LastModifiedDate = DateTime.Now,
                IsDisplay = true,
                DisplayOrder = 6
            };
            return new[]
            {
                DataTypeText,
                DataTypeTextLimited,
                DataTypeInt,
                DataTypeDecimal,
                DataTypeDatetime,
                DataTypeBit
            }.ToList();
        }
    }
}
