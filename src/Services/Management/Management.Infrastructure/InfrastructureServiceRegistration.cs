﻿using Management.Infrastructure.Repositories.Commons;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Management.Infrastructure.Persistence;
using System.Configuration;
using Management.Application.Contracts.Persistence.Organizations;
using Management.Infrastructure.Repositories.Organizations;
using Management.Application.Contracts.Persistence.Definitions;
using Management.Infrastructure.Repositories.Definitions;
using Management.Application.Contracts.Persistence.Parameters;
using Management.Infrastructure.Repositories.Parameters;
using Management.Application.Services.Parameters;
using Management.Infrastructure.Services.Parameters;
using Management.Application.Contracts.Persistence.Commons;
using Management.Application.Contracts.Persistence.Authentications;

namespace Management.Infrastructure
{
    public static class InfrastructureServiceRegistration
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddScoped<IOrganizationRepository, OrganizationRepository>();
            services.AddScoped<IModuleRepository, ModuleRepository>();
            services.AddScoped<IModuleTypeRepository, ModuleTypeRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IRolePermissionRepository, RolePermissionRepository>();

            #region Parameters


            services.AddScoped<IParameterRepository, ParameterRepository>();
            services.AddScoped<IParameterService, ParameterService>();

            services.AddScoped<IParameterSystemTypeRepository, ParameterSystemTypeRepository>();
            services.AddScoped<IParameterSystemTypeService, ParameterSystemTypeService>();

            #endregion


            return services;
        }
    }
}
