﻿using Management.Domain.Configurations.Authentications;
using Management.Domain.Configurations.Definitions;
using Management.Domain.Configurations.Organizations;
using Management.Domain.Configurations.Parameters;
using Management.Domain.Entities.Concrete.Authentications;
using Management.Domain.Entities.Concrete.Commons;
using Management.Domain.Entities.Concrete.Definitions;
using Management.Domain.Entities.Concrete.Organizations;
using Management.Domain.Entities.Concrete.Parameters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Infrastructure.Persistence
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedDate = DateTime.Now;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Authentications

            modelBuilder.ApplyConfiguration(new PermissionConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());

            #endregion

            #region Definitions

            modelBuilder.ApplyConfiguration(new DataTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LanguageConfiguration());
            modelBuilder.ApplyConfiguration(new ModuleConfiguration());
            modelBuilder.ApplyConfiguration(new ModuleTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OperationTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ParameterTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SystemTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TranslateConfiguration());

            #endregion

            #region Organizations

            modelBuilder.ApplyConfiguration(new OrganizationConfiguration());
            //modelBuilder.ApplyConfiguration(new OrganizationModuleConfiguration());



            #endregion

            #region Parameters

            modelBuilder.ApplyConfiguration(new ParameterConfiguration());
            modelBuilder.ApplyConfiguration(new ParameterSystemTypeConfiguration());




            #endregion

            foreach (var e in modelBuilder.Model.GetEntityTypes())
            {
                foreach (var fk in e.GetForeignKeys())
                {
                    fk.DeleteBehavior = DeleteBehavior.Restrict;
                }
            }
        }

        #region Authentications

        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<User> Users { get; set; }

        #endregion

        #region Definitions

        public DbSet<DataType> DataTypes { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ModuleType> ModuleTypes { get; set; }
        public DbSet<OperationType> OperationTypes { get; set; }
        public DbSet<ParameterType> ParameterTypes { get; set; }
        public DbSet<SystemType> SystemTypes { get; set; }
        public DbSet<Translate> Translates { get; set; }

        #endregion

        #region Organizations

        public DbSet<Organization> Organizations { get; set; }
        //public DbSet<OrganizationModule> OrganizationModules { get; set; }


        #endregion

        #region Parameters

        public DbSet<Domain.Entities.Concrete.Parameters.Parameter> Parameters { get; set; }
        public DbSet<ParameterSystemType> ParameterSystemTypes { get; set; }


        #endregion






    }
}
