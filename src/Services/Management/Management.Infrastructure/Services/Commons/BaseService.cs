﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Application.Services.Commons;
using Management.Domain.Entities.Concrete.Abstarct.Commons;
using Management.Domain.Entities.Concrete.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Services.Commons
{
    public partial class BaseService<TEntity> : IBaseService<TEntity> where TEntity : BaseEntity
    {
        #region Fields

        private readonly IBaseRepository<TEntity> _entityRepository;

        #endregion

        #region Ctor

        public BaseService(IBaseRepository<TEntity> entityRepository)
        {
            _entityRepository = entityRepository;
        }

        #endregion

        #region Methods

        #region Insert

        /// <summary>
        /// Inserts a entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual Task<TEntity> AddEntityAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            //entity.DisplayOrder = CountEntitiesAsync().Result + 1;

            return _entityRepository.AddEntityAsync(entity);

        }

        /// <summary>
        /// Inserts a entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void AddEntitiesAsync(List<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            _entityRepository.AddEntitiesAsync(entities);
        }

        #endregion

        #region Select

        /// <summary>
        /// Gets a entity
        /// </summary>
        /// <param name="entityId">The entity identifier</param>
        /// <returns>Entity</returns>
        public virtual async Task<TEntity> GetEntityById(int entityId)
        {
            if (entityId == 0)
                return null;

            return await _entityRepository.GetEntityByIdAsync(entityId);
        }

        public virtual async Task<IEnumerable<TEntity>> GetEntitiesByIds(int[] entityIds)
        {
            return await _entityRepository.GetEntitiesAsync(p => entityIds.Contains(p.Id));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public virtual IPagedList<TEntity> GetPagedEntities(Expression<Func<TEntity, bool>> predicate = null, string includeString = null, int pageIndex = 0, int pageSize = int.MaxValue, Sort sort = null)
        {
            var query = _entityRepository.TableNoTracking;

            if (sort != null)
                query = query.Sort(sort);
            else
                query = query.OrderBy(c => c.Id);

            return new PagedList<TEntity>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<int> CountEntitiesAsync()
        {
            return await _entityRepository.CountEntitiesAsync();
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes a entity
        /// </summary>
        /// <param name="entitiy">The entitiy</param>
        public virtual void DeleteEntityAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entityRepository.DeleteEntityAsync(entity);


        }

        /// <summary>
        /// Deletes entities
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void DeleteEntitiesAsync(IList<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            _entityRepository.DeleteEntitiesAsync(entities);
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void UpdateEntityAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entityRepository.UpdateEntityAsync(entity);

        }

        /// <summary>
        /// Updates the entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void UpdateEntitiesAsync(List<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            _entityRepository.UpdateEntitiesAsync(entities);
        }

        #region Sort

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="entity"></param>
        //public virtual void DownEntity(TEntity entity )
        //{
        //    if (entity == null)
        //        throw new ArgumentNullException(nameof(entity));

        //    var nextEntity = _entityRepository.TableNoTracking.Where(c => c.DisplayOrder > entity.DisplayOrder).OrderBy(c => c.DisplayOrder).FirstOrDefault();
        //    if (nextEntity != null)
        //    {
        //        int entityDisplayOrder = entity.DisplayOrder;
        //        entity.DisplayOrder = nextEntity.DisplayOrder;
        //        nextEntity.DisplayOrder = entityDisplayOrder;

        //        UpdateEntityAsync(entity);
        //        UpdateEntityAsync(nextEntity);
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="entity"></param>
        //public virtual void UpEntity(TEntity entity)
        //{
        //    if (entity == null)
        //        throw new ArgumentNullException(nameof(entity));

        //    var nextEntity = _entityRepository.TableNoTracking.Where(c => c.DisplayOrder < entity.DisplayOrder).OrderByDescending(c => c.DisplayOrder).FirstOrDefault();
        //    if (nextEntity != null)
        //    {
        //        int entityDisplayOrder = entity.DisplayOrder;
        //        entity.DisplayOrder = nextEntity.DisplayOrder;
        //        nextEntity.DisplayOrder = entityDisplayOrder;

        //        UpdateEntityAsync(entity);
        //        UpdateEntityAsync(nextEntity);
        //    }
        //}

        #endregion

        #endregion

        #endregion

    }
}
