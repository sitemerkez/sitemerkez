﻿using Management.Application.Contracts.Persistence.Commons;
using Management.Application.Contracts.Persistence.Parameters;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using Management.Infrastructure.Services.Commons;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Management.Infrastructure.Services.Parameters
{
    public class ParameterService : BaseService<Parameter>, IParameterService
    {
        private readonly IBaseRepository<Parameter> _entityRepository;
        public ParameterService(IBaseRepository<Parameter> entityRepository) :base(entityRepository)
        {
            _entityRepository = entityRepository ?? throw new ArgumentNullException(nameof(entityRepository));

        }

        #region Select


        public async Task<IEnumerable<Parameter>> GetParametersListByOrganizationId(int organizationId)
        {
            return await _entityRepository.GetEntitiesAsync(x => x.OrganizationId == organizationId );
        }

        #endregion

        

    }
}
