﻿using Management.Application.Contracts.Persistence.Parameters;
using Management.Application.Services.Parameters;
using Management.Domain.Entities.Concrete.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Infrastructure.Services.Parameters
{
    public class ParameterSystemTypeService : IParameterSystemTypeService
    {
        private readonly IParameterSystemTypeRepository _parameterSystemTypeRepository;
        private readonly IParameterService _parameterService;
        public ParameterSystemTypeService(IParameterSystemTypeRepository parameterSystemTypeRepository, IParameterService parameterService)
        {
            _parameterSystemTypeRepository = parameterSystemTypeRepository ?? throw new ArgumentNullException(nameof(parameterSystemTypeRepository));
            _parameterService = parameterService ?? throw new ArgumentNullException(nameof(parameterService));


        }

        #region Insert

        public async Task<ParameterSystemType> AddEntityAsync(ParameterSystemType entity)
        {
            //cache
            return await _parameterSystemTypeRepository.AddEntityAsync(entity);
        }

        #endregion

        #region Select

        public async Task<ParameterSystemType> GetEntityByIdAsync(int id)
        {
            return await _parameterSystemTypeRepository.GetEntityByIdAsync(id);
        }

        public async Task<IEnumerable<ParameterSystemType>> GetParameterSystemTypesListByOrganizationId(int organizationId)
        {
            var parameterIdsList = _parameterService.GetParametersListByOrganizationId(organizationId).Result.ToList().Select(p => p.Id);

            return await _parameterSystemTypeRepository.GetEntitiesAsync(x => parameterIdsList.Contains(x.ParameterId));
        }

        public async Task<IEnumerable<ParameterSystemType>> GetParameterSystemTypesListByOrganizationIdAndSystemTypeId(int organizationId, int systemTypeId)
        {
            var parameterIdsList = _parameterService.GetParametersListByOrganizationId(organizationId).Result.ToList().Select(p => p.Id);

            return await _parameterSystemTypeRepository.GetEntitiesAsync(x => x.SystemTypeId == systemTypeId && parameterIdsList.Contains(x.ParameterId));
        }

        #endregion

        #region Update

        public async Task UpdateEntityAsync(ParameterSystemType entity)
        {
            await _parameterSystemTypeRepository.UpdateEntityAsync(entity);
        }


        #endregion

        #region Delete

        public async Task DeleteEntityAsync(int id)
        {
            var parameterSystemTypeEntity = _parameterSystemTypeRepository.GetEntityByIdAsync(id).Result;
            await _parameterSystemTypeRepository.DeleteEntityAsync(parameterSystemTypeEntity);
        }

        #endregion

    }
}

