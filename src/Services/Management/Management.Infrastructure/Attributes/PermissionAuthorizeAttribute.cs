﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Management.Infrastructure.Attributes
{
    public class PermissionAuthorizeFilter : Attribute, IActionFilter
    {
        private readonly string _permissionName;

        public PermissionAuthorizeFilter(string permissionName)
        {
            _permissionName = permissionName;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
          


        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            //bool response = false;

            //var _permissionRepository = context.HttpContext.RequestServices.GetService<IPermissionRepository>();
            //var _rolePermissionRepository = context.HttpContext.RequestServices.GetService<IRolePermissionRepository>();
            //var _sharedIdentityService = context.HttpContext.RequestServices.GetService<ISharedIdentityService>();

            //var roleIds = _sharedIdentityService.GetRoleIds;



            //var permission = _permissionRepository.GetEntitiesAsync(x => x.Name == _permissionName).Result;

            //if (permission != null)
            //{
            //    foreach (var roleItem in roleIds)
            //    {
            //        var roleId = roleItem.Split("roleIds:").LastOrDefault().Trim();
            //        var rolePermission = _rolePermissionRepository.GetEntitiesAsync(x => x.RoleId == roleId && x.PermissionId == permission.Id).Result;

            //        if(rolePermission != null)
            //        {
            //            response = true;
            //            break;
            //        }
            //    }
            //}
            //else
            //{
            //    response = false;
            //}

            //if(response == false)
            //{
            //    throw new Exception("Not authorized");
            //}

        }
    }
}
