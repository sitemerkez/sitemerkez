﻿using Catalog.Api.Entities;
using MongoDB.Driver;

namespace Catalog.Api.Data
{
    public interface ICatalogContext
    {
        IMongoCollection<Category> Categories { get; }

        IMongoCollection<Attribute> Attributes { get; }

        IMongoCollection<AttributeValue> AttributeValues { get; }
        IMongoCollection<Product> Products { get; }
    }
}
