﻿using Catalog.Api.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Catalog.Api.Data
{
    public class CatalogContext : ICatalogContext
    {
        public CatalogContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));


            Categories = database.GetCollection<Category>(configuration.GetValue<string>("DatabaseSettings:CategoryCollectionName"));
            Attributes = database.GetCollection<Attribute>(configuration.GetValue<string>("DatabaseSettings:AttributeCollectionName"));
            AttributeValues = database.GetCollection<AttributeValue>(configuration.GetValue<string>("DatabaseSettings:AttributeValueCollectionName"));
            Products = database.GetCollection<Product>(configuration.GetValue<string>("DatabaseSettings:ProductCollectionName"));
            CatalogContextSeed.SeedData(Products);
        }

        public IMongoCollection<Category> Categories { get; }
        public IMongoCollection<Attribute> Attributes { get; }
        public IMongoCollection<AttributeValue> AttributeValues { get; }
        public IMongoCollection<Product> Products { get; }
    }
}
