﻿namespace Catalog.Api.Entities
{
    public class Image
    {
        public string Url { get; set; }
    }
}
