﻿namespace Catalog.Api.Entities
{
    public class ProductAttribute
    {
        public int AttributeId { get; set; }    
        public int AttributeValueId { get; set; }
        public string customAttributeValue { get; set; } 
    }
}
