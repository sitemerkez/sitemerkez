import { Module } from '../../models/definitions/module';

export class ModuleRepository {
  private modules: Module[];

  constructor() {
    // this.modules = [
    //   {
    //     id: 1,
    //     name: 'Kullanici',
    //     Description: 'Kullanıcı Modülü',
    //     ModuleTypeId: 1,
    //     ModuleTypeName: 'User',
    //     IsDisplay: true,
    //     CreatedOn: new Date('2021-01-01'),
    //   },
    //   {
    //     Id: 2,
    //     Name: 'İnsan Kaynakları',
    //     Description: 'İnsan Kaynakları Modülü',
    //     ModuleTypeId: 2,
    //     ModuleTypeName: 'HR',
    //     IsDisplay: true,
    //     CreatedOn: new Date('2021-01-01'),
    //   },
    //   {
    //     Id: 4,
    //     Name: 'CRM',
    //     Description: 'CRM Modülü',
    //     ModuleTypeId: 3,
    //     ModuleTypeName: 'CRM',
    //     IsDisplay: true,
    //     CreatedOn: new Date('2021-01-01'),
    //   },
    //   {
    //     Id: 3,
    //     Name: 'Muhasebe',
    //     Description: 'Muhasebe Modülü',
    //     ModuleTypeId: 4,
    //     ModuleTypeName: 'Accouting',
    //     IsDisplay: true,
    //     CreatedOn: new Date('2021-01-01'),
    //   },
    //   {
    //     Id: 5,
    //     Name: 'E-Ticaret',
    //     Description: 'E-Ticaret Modülü',
    //     ModuleTypeId: 5,
    //     ModuleTypeName: 'EShopping',
    //     IsDisplay: true,
    //     CreatedOn: new Date('2021-01-01'),
    //   },
    // ];
  }

  getModulesList(): Module[] {
    return this.modules;
  }

  getModuleById(id: number): Module | undefined {
    return this.modules.find((i) => i.id == id);
  }

  getModulesByFilterText(filterText: string): Module[] {
    if (filterText == '') {
      return this.getModulesList();
    }
    return this.modules.filter(
      (m: Module) =>
        m.name.toLocaleLowerCase().indexOf(filterText) !== -1 ||
        m.description.toLocaleLowerCase().indexOf(filterText) !== -1
    );
  }
}
