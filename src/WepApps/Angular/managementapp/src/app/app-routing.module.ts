import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { ModulesComponent } from './modules/modules.component';
import { ModuleDetailsComponent } from './modules/module-details/module-details.component';
import { ModuleCreateComponent } from './modules/module-create/module-create.component';

const routes: Routes = [
  { path: 'modules', component: ModulesComponent},
  { path:'', redirectTo:'modules', pathMatch:'full'},
  { path:'modules/create',  component:ModuleCreateComponent},
  { path:'modules/:moduleId',  component:ModuleDetailsComponent}

]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
