export const environment = {
	production: false,
	isLogConsole: true,
	apiUrl: 'https://localhost:5000/services',
	tokenRefreshInterval: 3600, // second
    authTokenKey: '22E126D4-DE59-4C5F-B9D5-B1522951ADC9'
};
