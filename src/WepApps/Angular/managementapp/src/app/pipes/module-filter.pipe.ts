import { Pipe, PipeTransform } from '@angular/core';
import { Module} from '../models/definitions/module';
import { ModuleRepository } from 'src/app/repository/definitions/module.repository';


@Pipe({
  name: 'moduleFilter'
})


export class ModuleFilterPipe implements PipeTransform {

  moduleRepository : ModuleRepository;
  
  /**
   *
   */
  constructor() {
    this.moduleRepository = new ModuleRepository();

  }

  transform(modules: Module[], filterText: string): Module[] {
    
    filterText = filterText.toLocaleLowerCase();

    modules = this.moduleRepository.getModulesByFilterText(filterText);

    // console.log(filterText);
    // console.log(modules);


    return modules;
  }

}
