


export class Module 
{
    id :number;
    name : string;
    description : string;
    moduleTypeId : number;
    moduleTypeName : string;
    isDisplay : boolean;
    createdOn : Date;
}