import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { LeftmenuComponent } from './leftmenu/leftmenu.component';
import { ModuleDetailsComponent } from './modules/module-details/module-details.component';
import { ModuleComponent } from './modules/module/module.component';
import { ModulesComponent } from './modules/modules.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SummaryPipe } from './pipes/summary.pipe';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { ModuleFilterPipe } from './pipes/module-filter.pipe';
import { AlertifyService } from './services/commons/alertify.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BaseService } from './services/commons/base.service';
import { AppRoutingModule } from './app-routing.module';
import { ModuleCreateComponent } from './modules/module-create/module-create.component';

@NgModule({
  declarations: [   //component
    AppComponent,
    NavbarComponent,
    LeftmenuComponent,
    ModulesComponent,
    ModuleComponent,
    ModuleDetailsComponent,
    FooterComponent,
    TopmenuComponent,
    SummaryPipe,
    ModuleFilterPipe,
    ModuleCreateComponent 
  ],
  imports: [       //module
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AlertifyService,
    BaseService
  ],    //services
  bootstrap: [AppComponent] //startcomponent
})
export class AppModule { }

