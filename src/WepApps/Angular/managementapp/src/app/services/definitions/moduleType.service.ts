import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ModuleType } from '../../models/definitions/moduleType';
import { environment } from '../../environments/environment';

import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class ModuleTypeService {
  constructor(private http: HttpClient) {}

  getModuleTypeFromObject(filterText : string): Observable<ModuleType[]> {
   
    if(filterText == '')
    {
      filterText = '%20';
    } 

    return this.http.get<ModuleType[]>(environment.apiUrl + '/Management/ModuleType/GetModuleTypesListByFilterText/'+ filterText)
  }

  getModuleTypeById(id : number)
  {
    return this.http.get<ModuleType>(environment.apiUrl + '/Management/ModuleType/GetModuleTypeById/'+ id)
  }
}
