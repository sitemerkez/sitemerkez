import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Module } from '../../models/definitions/module';
import { environment } from '../../environments/environment';

import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class ModuleService {
  constructor(private http: HttpClient) {}

  getModuleFromObject(filterText : string): Observable<Module[]> {
   
    if(filterText == '')
    {
      filterText = '%20';
    } 

    return this.http.get<Module[]>(environment.apiUrl + '/Management/Module/GetModulesListByFilterText/'+ filterText)
  }

  getModuleById(id : number)
  {
    return this.http.get<Module>(environment.apiUrl + '/Management/Module/GetModuleById/'+ id)
  }
}
