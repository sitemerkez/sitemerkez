import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';


const headers= new HttpHeaders();
//   .set('content-type', 'application/json')
//   .set('Access-Control-Allow-Origin', '*');

    

  
@Injectable()
export class BaseService {

    constructor(public http: HttpClient) { }


    jwt_header() {
        // create authorization header with jwt token
        // let currentUser = null; //Helpers.GetCurrentUser();
        // if (currentUser ) {
        //     return new HttpHeaders({ 'Authorization': 'Bearer ' +"" });
        // }


        return headers;
    }

    // repository services
    getAll(apiControllerName: string, metot: string) {
        return this.http.get<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot, { headers: this.jwt_header() });
    }

    getById(apiControllerName: string, metot: string, id: number) {
        return this.http.get<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot + '/' + id, { headers: this.jwt_header() });
    }

    create(apiControllerName: string, metot: string, entity: any) {
        return this.http.post<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot, entity, { headers: this.jwt_header() });
    }

    update(apiControllerName: string, metot: string, entity: any) {
        return this.http.put<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot + '/' + entity.id, entity, { headers: this.jwt_header() });
    }

    delete(apiControllerName: string, metot: string, id: number) {
        return this.http.delete<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot + '/' + id, { headers: this.jwt_header() });
    }

    getByParams(apiControllerName: string, metot: string, params_: any) {
        return this.http.get<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot, { params: params_, headers: this.jwt_header() });
    }

    getByFileId(apiControllerName: string, metot: string, id: number) {
        return this.http.get(environment.apiUrl + '/' + apiControllerName + '/' + metot + '/' + id, { headers: this.jwt_header(), responseType: 'blob' });
    }

    uploadFile(apiControllerName: string, metot: string, formData: any) {
        return this.http.post<any>(environment.apiUrl + '/' + apiControllerName + '/' + metot, formData, { headers: this.jwt_header() });
    }

    birimTreeList(id: number) {
        return this.http.get<any>(environment.apiUrl + '/Kullanici/GetTree/' + id, { headers: this.jwt_header() });
    }
}
