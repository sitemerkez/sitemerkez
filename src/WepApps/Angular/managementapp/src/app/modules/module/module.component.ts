import { Component, OnInit } from '@angular/core';
import { Module } from 'src/app/models/definitions/module';
import { ModuleRepository } from 'src/app/repository/definitions/module.repository';
import { AlertifyService } from 'src/app/services/commons/alertify.service';
import { ModuleService } from 'src/app/services/definitions/module.service';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css'],
  providers :[ ModuleService ]
})
export class ModuleComponent implements OnInit {
  title = 'Modül Listesi';
  modulesList: Module[] = [];
  moduleRepository: ModuleRepository;

  filterText: string = "";
  error : any

  constructor(
    private alertify: AlertifyService, 
    private moduleService : ModuleService
    ) {
    
  }

  refresh() {
    this.moduleService.getModuleFromObject(this.filterText)
      .subscribe(data => {
        this.modulesList=data;
      }, error => this.error = error)      
 
  }

  ngOnInit(): void {
    this.refresh();
  }

  onInputChange() {

    this.refresh();
  }

  onKeydown() {
    
    this.refresh();
  }

  onClear(){
    this.filterText = "";
    this.refresh();
  }


  addToList($event: any, module: Module) {
   if($event.target.classList.contains('btn-primary'))
   {
    $event.target.innerText = "Listeden Çıkar";
    $event.target.classList.remove('btn-primary');
    $event.target.classList.add('btn-danger');

    this.alertify.success(module.name + 'listene eklendi');
   }
   else
   {
    $event.target.innerText = "Listeden Ekle";
    $event.target.classList.remove('btn-danger');
    $event.target.classList.add('btn-primary');

    this.alertify.error(module.name + 'listeden çıkarıldı');
   }
  }
}
