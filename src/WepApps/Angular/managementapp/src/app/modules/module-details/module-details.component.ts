import { Component, OnInit } from '@angular/core';
import { Module } from '../../models/definitions/module';
import { ModuleService } from 'src/app/services/definitions/module.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-module-details',
  templateUrl: './module-details.component.html',
  styleUrls: ['./module-details.component.css'],
  providers: [ModuleService],
})
export class ModuleDetailsComponent implements OnInit {
  module: Module;
  constructor(private moduleService: ModuleService,
              private activatedRoute: ActivatedRoute) {}


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params=> {
      this.moduleService.getModuleById(params["moduleId"]).subscribe(data => {
        this.module = data;
      })
    })
  }
}
