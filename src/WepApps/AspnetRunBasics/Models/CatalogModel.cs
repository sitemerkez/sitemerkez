﻿using System.Collections.Generic;

namespace AspnetRunBasics.Models
{
    public class CatalogModel
    {
        public string Id { get; set; }

        public int OrganizationId { get; set; }

        public string Barcode { get; set; }

        public string Name { get; set; }

        public string ProductMainId { get; set; }

        public int BrandId { get; set; }

        public string CategoryId { get; set; }
        public string StockCode { get; set; }
        public int DimensionalWeight { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }

        public List<Image> Images { get; set; }
        public List<ProductAttribute> Attributes { get; set; }

        public string CurrencyType { get; set; }

        public decimal ListPrice { get; set; }

        public decimal SalePrice { get; set; }

        public int DeliveryDuration { get; set; }

        public int VatRate { get; set; }
    }
}
